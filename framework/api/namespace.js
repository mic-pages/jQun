﻿new function(window, Namespace){

this.Web = (function(){
	return new Namespace(
		{
			Both : new Namespace(),
			Common : new Namespace(
				{
					Framework : new Namespace(
						{
							API : new Namespace().members({
								CallServer : new Namespace(),
								Site : new Namespace()
							}),
							UI : new Namespace()
						}
					),
					Project : new Namespace()
				}
			),
			Computer : new Namespace(),
			Mobile : new Namespace(),
			Site : new Namespace()
		}
	);
}());

Namespace.prototype.members.call(window, this);
}(
	window,
	jQun.Namespace
);