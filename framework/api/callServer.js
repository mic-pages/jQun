﻿new function(CallServer, StaticClass, Interface, Ajax, fullname){

this.Save = (function(open, dataFormat, set){
	function Save(){
		Ajax.override({
			open : function(name, parameters, callback, _error, _beforeSend, _progress){
				open.call(
					this,
					name,
					parameters,
					function(response){
						if(
							response === null
						){
							alert("不存在的访问（" + name + "）。");
							return;
						}

						if(
							response.status === 0
						){
							alert(response.description);
							
							if(
								typeof _error === "function"
							){
								_error();
							}

							return;
						}

						if(
							typeof callback !== "function"
						){
							return;
						}

						callback(name in dataFormat ? dataFormat[name](response.data) : response.data);
					},
					function(status){
						alert("操作错误，代码：" + status);

						if(
							typeof _error !== "function"
						){
							return;
						}

						_error();
					},
					_beforeSend,
					_progress
				);

				return this;
			}
		});
	};
	Save = new StaticClass(Save, fullname("Save"));

	Save.props({
		addFormats : function(formats){
			set(dataFormat, formats);

			return this;
		}
	});

	return Save;
}(
	Ajax.open,
	// dataFormat
	{},
	jQun.set
));

CallServer.members(this);
}(
	Web.Common.Framework.API.CallServer,
	jQun.StaticClass,
	jQun.Interface,
	jQun.Ajax,
	// fullname
	function(name){
		return "Web.Common.Framework.API.CallServer." + name;
	}
);