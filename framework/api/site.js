﻿new function(Site, StaticClass, Enum, Text, HTMLElementList, location, path, fullname){

this.Page = (function(Socket, JSON, PageStatus, windowList, urlText, history, forEach, set, encodeURIComponent){
	function Page(){
		
	};
	Page = new StaticClass(Page, fullname("Page"));

	Page.props({
		back : function(){
			history.back();
			return this;
		},
		getHostUrl : function(_searchObject){
			return this.getUrl("", _searchObject, "");
		},
		getUrl : function(path, _searchObject, _file){
			var str = JSON.stringifyAddressParameters(_searchObject);

			return urlText.replace(
				set(
					{
						search : str.length > 0 ? "?" + str : ""
					},
					path ? {
							path : path + "/",
							file : _file || ""
						} : {
							path : "",
							file : ""
						}
				)
			);
		},
		go : function(path, _searchObject, _file){
			location.href = this.getUrl(path, _searchObject, _file);

			return this;
		},
		name : ""
	});

	return Page;
}(
	jQun.Socket,
	jQun.JSON,
	this.PageStatus,
	// windowList
	new HTMLElementList(window),
	// urlText
	new Text([
		location.protocol,
		"",
		location.host,
		[
			path[1],
			"{path}",
			"{?~file}",
			"{?~search}"
		].join("")
	].join("/")),
	history,
	jQun.forEach,
	jQun.set,
	encodeURIComponent
));

Site.members(this);
}(
	Web.Common.Framework.API.Site,
	jQun.StaticClass,
	jQun.Enum,
	jQun.Text,
	jQun.HTMLElementList,
	location,
	// path
	location.pathname.match(/((?:[^\/]+\/)*?)(?:[^\/]+\/)?(?:([^\/\.]+\.htm(?:l)?)?(?:\?[^\/\.]*)?)?$/),
	// fullname
	function(name){
		return "Web.Common.Framework.API.Site." + name;
	}
);