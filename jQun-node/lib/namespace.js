﻿require("./jQun.js");

new function(global, Namespace, UnopenedNamespace){

this.Web = (function(dirname){
	return new Namespace(
		{
			Both : new Namespace(),
			Common : new Namespace(),
			Site : new Namespace()
		}
	);
}(
	// dirname
	__dirname + "/"
));

Namespace.prototype.members.call(global, this);
}(
	global,
	jQun.Namespace,
	jQun.UnopenedNamespace
);