﻿new function(jQun, StaticClass, Enum, Event, defineProperties){

this.WeixinShareTypes = (function(){
	return new Enum(
		["SendAppMessage", "ShareTimeline", "ShareWeibo"]
	);
}());

this.Weixin = (function(
	HTMLElementList, Verification, WeixinJSBridge,
	WeixinShareTypes, VerificationRegExpString,
	title, link, description, imageUrl, appid,
	beforeweixinshareEvent, afterweixinshareEvent, window, document, location,
	set
){
	function Weixin(){
		var weixin = this;

		new HTMLElementList(
				document
			)
			.attach({
				WeixinJSBridgeReady : function(){
					WeixinJSBridge = window.WeixinJSBridge;
					weixin.isReady = true;

					WeixinJSBridge.on(
						'menu:share:appmessage',
						function(){
							weixin.sendAppMessage();
						}
					);
					
					WeixinJSBridge.on(
						"menu:share:timeline",
						function(){
							weixin.shareTimeline();	
						}
					);

					WeixinJSBridge.on(
						"menu:share:weibo",
						function(){
							weixin.shareWeibo();
						}
					);
				}
			});
	};
	Weixin = new StaticClass(Weixin, "jQun.Weixin");

	Weixin.props({
		appid : {
			get : function(){
				return appid || new HTMLElementList('meta[name="appid"]').getAttribute("content") || "";
			},
			set : function(value){
				appid = value;
			}
		},
		description : {
			get : function(){
				return description || new HTMLElementList('meta[name="description"]').getAttribute("content") || "";
			},
			set : function(value){
				description = value;
			}
		},
		imageUrl : {
			get : function(){
				if(
					imageUrl
				){
					return imageUrl;
				}
				
				var iconList = new HTMLElementList('link[rel~="icon"]');
				
				if(
					iconList.length > 0
				){
					var href = iconList.getAttribute("href");

					if(
						Verification.result(href, VerificationRegExpString.WebURL)
					){
						return href;
					}

					return document.baseURI + href;
				}

				return "";
			},
			set : function(value){
				imageUrl = value;
			}
		},
		link : {
			get : function(){
				return link || location.href;
			},
			set : function(value){
				link = value;
			}
		},
		title : {
			get : function(){
				return title || document.title;
			},
			set : function(value){
				title = value;
			}
		}
	},{
		settable : true, gettable : true
	});

	Weixin.props({
		call : function(){
			if(
				WeixinJSBridge
			){
				WeixinJSBridge.call.apply(WeixinJSBridge, arguments);
			}

			return this;
		},
		imageHeight : 400,
		imageWidth : 400,
		invoke : function(){
			if(
				WeixinJSBridge
			){
				WeixinJSBridge.invoke.apply(WeixinJSBridge, arguments);
			}

			return this;
		},
		isReady : false,
		sendAppMessage : function(){
			this.share(WeixinShareTypes.SendAppMessage);
		},
		setAppid : function(appid){
			this.appid = appid;

			return this;
		},
		setDescription : function(description){
			this.description = description;

			return this;
		},
		setImageSize : function(width, height){
			this.imageWidth = width;
			this.imageHeight = height;

			return this;
		},
		setImageUrl : function(url){
			this.imageUrl = url;
			
			return this;
		},
		setLink : function(link){
			this.link = link;

			return this;
		},
		setTitle : function(title){
			this.title = title;

			return this;
		},
		share : function(type, _more){
			if(
				!WeixinJSBridge
			){
				return this;
			}
			
			beforeweixinshareEvent.trigger(
				document,
				{
					type : type
				}
			);
			
			WeixinJSBridge.invoke(
				WeixinShareTypes.getNameByValue(type, true),
				set(
					_more || {},
					{
						img_url : this.imageUrl,
                        img_width : this.imageWidth,
                        img_height : this.imageHeight,
                        link : this.link,
                        desc : this.description,
                        title : this.title
					}
				),
				function(e) {
					if(
						e.err_msg.match(/\:\s*cancel\s*$/ig)
					){
						return;
					}

					afterweixinshareEvent.trigger(
						document,
						{
							type : type
						}
					);
				}
			);

			return this;
		},
		shareTimeline : function(){
			this.share(WeixinShareTypes.ShareTimeline);

			return this;
		},
		shareWeibo : function(){
			this.share(
				WeixinShareTypes.ShareWeibo,
				{
					content : this.title + " - " + this.description,
					url : this.link
				}
			);

			return this;
		}
	});

	return Weixin;
}(
	jQun.HTMLElementList,
	jQun.Verification,
	// WeixinJSBridge
	null,
	this.WeixinShareTypes,
	jQun.VerificationRegExpString,
	// title
	"",
	// link
	"",
	// description
	"",
	// imageUrl
	"",
	// appid
	"",
	// beforeweixinshareEvent
	new Event("beforeweixinshare"),
	// afterweixinshareEvent
	new Event("afterweixinshare"),
	window,
	document,
	location,
	jQun.set
));

defineProperties(jQun, this);
}(
	jQun,
	jQun.StaticClass,
	jQun.Enum,
	jQun.Event,
	jQun.defineProperties
);