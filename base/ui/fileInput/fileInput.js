﻿new function(jQun, Class, StaticClass, HTML, Event, defineProperties){

this.File = (function(FileReader, URL){
	function File(){
		///	<summary>
		///	关联文件处理的类。
		///	</summary>
	};
	File = new StaticClass(File, "jQun.File");

	File.props({
		revokeLocal : function(url){
			///	<summary>
			///	释放本地链接资源。
			///	</summary>
			///	<param name="url" type="String">指定的链接。</param>
			URL.revokeObjectURL(url);
		},
		toData : function(file, loaded){
			///	<summary>
			///	将文件转化为database64数据。
			///	</summary>
			///	<param name="file" type="window.File">需要转化的文件。</param>
			///	<param name="loaded" type="Function">转化成功后所指定的回调函数。</param>
			var fileReader = new FileReader();

			fileReader.onload = function(){
				loaded(this.result);
			};
			fileReader.readAsDataURL(file);

			return this;
		},
		toLocal : function(file){
			///	<summary>
			///	将文件转化为本地链接。
			///	</summary>
			///	<param name="file" type="window.File">需要转化的文件。</param>
			return URL.createObjectURL(file);
		}
	});

	return File;
}(
	FileReader,
	window.URL || webkitURL
));

this.FileInput = (function(HTMLElementList, fileselectedEvent, every){
	function FileInput(selector, typeRegx, description){
		///	<summary>
		///	选择文件类。
		///	</summary>
		///	<param name="selector" type="String, HTMLElementList">元素选择器。</param>
		///	<param name="typeRegx" type="RegExp">规定文件类型的正则表达式。</param>
		///	<param name="description" type="String">错误类型文件的提示描述文字。</param>
		var inputFile = this;

		this.attach({
			change : function(e){
				var files = this.files;

				if(
					files.length === 0
				){
					return;
				}

				if(
					every.call(
						files,
						function(file){
							if(
								file.name.match(typeRegx)
							){
								return true;
							}

							alert(description);
							return false;
						}
					)
				){
					fileselectedEvent.trigger(
						this,
						{
							file : files[0],
							files : files
						}
					);
				}

				this.value = "";
			}
		});
	};
	FileInput = new Class(FileInput, "jQun.FileInput", HTMLElementList.prototype);

	return FileInput.constructor;
}(
	jQun.HTMLElementList,
	// fileselectedEvent
	new Event("fileselected"),
	jQun.every
));

this.ImageFileInput = (function(FileInput, IMAGE_REGEXPS, html){
	function ImageFileInput(_title){
		///	<summary>
		///	选择图像文件类。
		///	</summary>
		///	<param name="_title" type="String">鼠标hover状态下的标题。</param>
		this.combine(
			html.create(
				{ title : _title }
			)
		);

		new FileInput(
			this.query(">input")[0],
			IMAGE_REGEXPS,
			"请选择正确的图像文件！"
		);
	};
	ImageFileInput = new Class(ImageFileInput, "jQun.ImageFileInput", FileInput.prototype);

	return ImageFileInput.constructor;
}(
	this.FileInput,
	// IMAGE_REGEXPS
	/\.(png|jpg|jpeg|bmp|gif)$/i,
	// html
	new HTML([
		'<div class="imageFileInput" title="{?~title}">',
			'<input type="file" accept="image/*" />',
		'</div>'
	].join(""))
));

defineProperties(jQun, this);
}(
	jQun,
	jQun.Class,
	jQun.StaticClass,
	jQun.HTML,
	jQun.Event,
	jQun.defineProperties
);