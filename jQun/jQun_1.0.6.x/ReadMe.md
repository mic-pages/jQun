﻿1.0.6.6 增加枚举类。

1.0.6.5 修改参数描述。

1.0.6.4 对jQun、NonstaticClass、Static增加toString方法。

1.0.6.3 ElementList类，增加获取元素结构的方法：header、article、section、footer等。

1.0.6.2 HTML类，修改参数：允许传入字符串模板和元素标签，如果是元素标签，则取innerHTML作为字符串模板

1.0.6.1 ElementList类，增加focus和blur方法

1.0.6.0 修复jQun.Browser的iphone版本匹配错误，增加字段：marjorVersion，用于显示主要版本信息。