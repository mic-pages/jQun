﻿1.1.2.9 给Namespace增加参数：_members，用于初始化成员。

1.1.2.8 给CHTML增加事件，当组件初始完成时触发componentready事件，事件无特殊参数。

1.1.2.7 给NodeList的remove方法加保护；转义HTML模板循环内的单引号。

1.1.2.6 修复List类contact方法的bug；修复define的修饰符bug。

1.1.2.5 Ajax请求关键字始终采用地址栏传输，不管是post或get；并且完善AttributesCollection类contains方法：只判断第一个元素。

1.1.2.4 Ajax增加可以修改请求关键字的功能。

1.1.2.3 修改Verification.every方法的返回值为索引;增加ICache、Cache、LocalCache等2个与缓存相关的接口、类，并优化大数据性能。

1.1.2.2 JSON增加方法parseAddressParameters，以获取地址栏参数对象；修改Vertification类，增加和完善匹配。

1.1.2.1 增加方法Port.receive，用于接收消息；增加CrossWindowPort、TransferPort，用于跨窗口通信。

1.1.2.0 优化Event、EventTargetList代码，使逻辑更清晰；新增OverrideDispatch类；Socket与Port支持元素之间的通信。