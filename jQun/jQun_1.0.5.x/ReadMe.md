﻿1.0.5.9 ElementList增加removeAttribute方法

1.0.5.8 HTMLElementList.prototype添加：getAttribute、getCSSPropertyValue、setAttribute、setCSSPropertyValue

1.0.5.7 优化NodeList.prototype.attach，为jQun(e.target)提供简便方式；优化StaticHTML，type格式必须为"text/staticHtml"。

1.0.5.6 增加StaticHTML类，用于静态模板。

1.0.5.5 NodeList.attach增加参数：支持addEventListener的所有参数;优化RequestConnection的传参。

1.0.5.4 增加Validation类，用于验证

1.0.5.3 Event类大改，由原来1次性永久性事件改为每次触发产生新的事件，但是类的实例仍不变（主要解决了事件冒泡被阻止一次后，事件就不再冒泡的问题）。

1.0.5.2 Event.attachTo的参数为"*"的话，将给HTMLElmentList添加此事件；Browser修复手机chrome的信息判断。

1.0.5.1 基类的assign方法修改，不支持未定义的值，目的是减少赋值的保护判断：当赋值的时候，如果值为undefined，则跳过，进行下个属性的赋值

1.0.5.0 Event.attachTo支持单个元素；开放RequestStorage类，并更名为Storage类，用于数据操作。