﻿2.0.0
	1. 针对Chrome 42版本，进行全面改版：去除自定义类名称，如：new Class(constructor, "name", SuperClass) 改为 new Class(constructor, SuperClass)；
	2. 优化类的代码，提高运行效率；
 	3. 其他逻辑均不变。