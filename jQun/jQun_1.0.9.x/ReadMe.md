﻿1.0.9.6 将所有properties改为缩写props；将find方法改为query方法。

1.0.9.5 优化继承的性能，提高了2.5倍。

1.0.9.4 增加CHTML。

1.0.9.3 枚举，getNameByValue方法支持返回驼峰式的值。

1.0.9.2 HTML、StaticHTML类增加noscript标签的识别；优化HTML类，使其能支持简单js计算而且能进行简单log调试数据。

1.0.9.1 Validation类改名为Verification。

1.0.9.0 修复手机验证正则bug；引用第三种闭包方式。