﻿1.0.8.9 增加EventTargetList类，NodeList从此继承该类；
		主要目的是提供个更方便、更轻量的事件驱动类（不用再为了事件去继承较重的NodeList甚至HTMLElementList）。

1.0.8.8 所有默认值从undefined改为null。

1.0.8.7 解决Text替换值为null或undefined的bug；ElementList类解决获取不到html标签的bug。

1.0.8.6 Browser类：移除getMajorVersion方法，可以用parseInt来实现此方法；增加getMobileOSVersion方法。

1.0.8.5 增加AttributeCollection的toggle方法；并且修改set方法，默认值为空字符串。

1.0.8.4 增加BrowserCores枚举、OSs枚举，并继续修改了Browser类，解决了版本不对应的bug。

1.0.8.3 增加BrowserAgents枚举，并根据此枚举修改了Browser类。

1.0.8.2 DOM类增加方法：getCSSNumberValue。

1.0.8.1 HTMLElementList：height、width方法转为属性(可以用等号设置其值)；去除get、set方法的老式兼容，将metric方法修改为get、set方式。

1.0.8.0 ElementPropertyCollection将继承至接口IElementPropertyCollection；修复safari上面sessionStorage的bug。；增加data相关方法和类。