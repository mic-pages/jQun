﻿1.1.0.9 ChildrenCellocetion对remove方法增加参数，可移除指定子元素；修复Ajax模拟数据的bug；

1.1.0.8 NodeList添加insertAfter方法；

1.1.0.7 去除List的contains方法，因为indexOf已经具有此功能；NodeList的hasChild方法更名为contains； 

1.1.0.6 代码可读性优化；修复ElementList的query方法bug（当参数为Node节点的时候，始终返回该Node节点集合）；修复btw方法返回值格式错误

1.1.0.5 优化HTMLElementList类btw方法的性能；规范文档架构。

1.1.0.4 解决IE11 userAgent 带来的浏览器判断不准确问题。

1.1.0.3 处理chrome在元素未添加到文档中之前，事件不冒泡。

1.1.0.2 去掉了HTMLElementList的快速定义事件功能（基本没用到过）。

1.1.0.1 修改继承接口后不能传参的bug；将console.errow替换为throw。

1.1.0.0 将between方法改为btw方法；增加isBtw方法；修改show、hide方法，与hidden属性关联；ElementList增加toggle方法；EventTarget增加dispatch方法。