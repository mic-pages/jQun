﻿/*
 *  类库名称 ：jQun
 *  中文释义 ：聚集在一起的千里马
 *  文档状态 ：1.1.1.6
 *  本次修改 ：修改HTML的create方法第二种参数模式的返回值，将其变为普通数组。
 *  开发浏览器信息 ：firefox 20.0+、chrome 26.0+、IE 10+、基于webkit的手机浏览器
 */

// jQun的定义
(function(){

this.jQun = (function(create){
	function jQun(cstctor, _ParentClass){
		///	<summary>
		///	生成一个继承指定父类的新生类。
		///	</summary>
		///	<param name="cstctor" type="Function">新生类的构造函数。</param>
		///	<param name="_ParentClass" type="jQun">需要继承的父类。</param>
		var callee = arguments.callee;

		with(
			callee
		){
			if(
				!isInstanceOf(this, callee)
			){
				return new HTMLElementList(arguments[0], arguments[1]);
			}

			cstctor.toString = toString;
		}
		
		return cstctor.prototype = create(
			_ParentClass || this.getOwnClass(),
			{
				constructor : {
					value : cstctor,
					writable : true,
					configurable : true
				}
			}
		);
	}

	return jQun;
}(
	Object.create
));

}.call(
	window
));


// 基本方法和属性的定义
new function(jQun, Object, Array){

this.every = (function(){
	return function(obj, fn, _this){
		///	<summary>
		///	确定对象的所有成员是否满足指定的测试。
		///	</summary>
		///	<param name="obj" type="Object">需要测试成员的对象。</param>
		///	<param name="fn" type="Function">用于测试对象成员的测试函数。</param>
		///	<param name="_this" type="*">指定测试函数的 this 对象。</param>
		if(
			obj instanceof Array
		){
			return obj.every(fn, _this);
		}

		var isNumber = typeof obj === "number";

		if(
			typeof _this !== "undefined"
		){
			fn = fn.bind(_this);
		}

		if(
			isNumber
		){
			obj = new Array(obj + 1).join(" ");
		}

		for(
			var o in obj
		){
			if(
				fn.apply(
					_this,
					isNumber ? [o - 0] : [obj[o], o, obj]
				)
			){
				continue;
			}

			return false;
		}
		return true;
	};
}());

this.forEach = (function(every){
	return function(obj, fn, _this){
		///	<summary>
		///	遍历对象的所有成员并对其执行指定操作函数。
		///	</summary>
		///	<param name="obj" type="Object">需要遍历的对象。</param>
		///	<param name="fn" type="Function">指定操作的函数。</param>
		///	<param name="_this" type="*">指定操作函数的 this 对象。</param>
		every(
			obj,
			function(){
				fn.apply(this, arguments);
			
				return true;
			},
			_this
		);

		return obj;
	};
}(
	this.every
));

this.define = (function(forEach, defineProperty){
	return function(obj, name, value, _descriptor){
		///	<summary>
		///	将属性添加到对象或修改现有属性的特性。
		///	</summary>
		///	<param name="obj" type="Object">对其添加或修改属性的对象。</param>
		///	<param name="name" type="String">需要添加或修改的属性名。</param>
		///	<param name="value" type="*">需要添加或修改的属性值。</param>
		///	<param name="_descriptor" type="Object">需要添加或修改的属性描述符。</param>
		var desc = { configurable : true, writable : true };

		forEach(
			_descriptor,
			function(d){
				desc[d] = _descriptor[d];
			}
		);

		if(
			_descriptor && !!(_descriptor.gettable || _descriptor.settable)
		){
			desc.get = value.get;
			desc.set = value.set;

			delete desc["writable"];
		}
		else{
			desc.value = value;
		}

		defineProperty(obj, name, desc);
		return obj;
	};
}(
	this.forEach,
	Object.defineProperty
));

this.defineProperties = (function(forEach, define){
	return function(obj, props, _descriptor){
		///	<summary>
		///	将一个或多个属性添加到对象，并/或修改现有属性的特性。
		///	</summary>
		///	<param name="obj" type="Object">对其添加或修改属性的对象。</param>
		///	<param name="props" type="Object">包含一个或多个属性的键值对。</param>
		///	<param name="_descriptor" type="Object">需要添加或修改的属性描述符。</param>
		forEach(
			props,
			function(value, name){
				define(obj, name, value, _descriptor);
			}
		);

		return obj;
	};
}(
	this.forEach,
	this.define
));

this.set = (function(forEach){
	return function(obj, props){
		///	<summary>
		///	添加或修改指定对象的属性。
		///	</summary>
		///	<param name="obj" type="Object">需要添加或修改属性的对象。</param>
		///	<param name="props" type="Object">需要添加或修改的属性集合。</param>
		forEach(
			props,
			function(val, name){
				obj[name] = val;
			}
		);

		return obj;
	};
}(
	this.forEach
));

this.except = (function(set, forEach){
	return function(obj, props){
		///	<summary>
		///	返回一个不包含所有指定属性名称的对象。
		///	</summary>
		///	<param name="obj" type="Object">需要排除属性的对象。</param>
		///	<param name="props" type="Array">需要排除的属性名称数组。</param>
		var result = set({}, obj);

		forEach(
			props,
			function(name){
				delete result[name];
			}
		);
		
		return result;
	}
}(
	this.set,
	this.forEach
));

this.isInstanceOf = (function(getPrototypeOf){
	return function(obj, constructor){
		///	<summary>
		///	判断对象是否为指定类构造函数的一级实例（即直接由该类实例化）。
		///	</summary>
		///	<param name="obj" type="Object">用于判断的实例对象。</param>
		///	<param name="constructor" type="Function">指定的类。</param>
		return getPrototypeOf(obj) === constructor.prototype;
	};
}(
	Object.getPrototypeOf
));

this.isPropertyOf = (function(every, getOwnPropertyDescriptor){
	return function(obj, property){
		///	<summary>
		///	检测对象自己是否具有指定属性或访问器。
		///	</summary>
		///	<param name="obj" type="Object">一个可能具有指定属性或访问器的对象。</param>
		///	<param name="property" type="*">用于检测的属性或访问器。</param>
		return !every(
			getOwnPropertyNames(obj),
			function(name){
				return every(
					this,
					function(n){
						return this[n] !== property;
					},
					getOwnPropertyDescriptor(obj, name)
				);
			},
			["value", "get", "set"]
		);
	};
}(
	this.every,
	Object.getOwnPropertyDescriptor
));

this.nesting = (function(forEach){
	return function(obj, fn, _this){
		///	<summary>
		///	将对象中的每个枚举元素进行再枚举并执行指定操作（双重嵌套的forEach）。
		///	</summary>
		///	<param name="obj" type="Object">需要嵌套枚举并执行指定操作的对象（一般为json）。</param>
		///	<param name="fn" type="Function">指定的操作函数。</param>
		///	<param name="_this" type="*">指定操作函数的 this 对象。</param>
		if(typeof fn === "undefined"){
			fn = fn.bind(_this);
		}

		forEach(
			obj,
			function(o){
				forEach(o, fn);
			}
		);

		return obj;
	};
}(
	this.forEach
));

this.toArray = (function(slice){
	return function(obj, _start, _end){
		///	<summary>
		///	将类似数组的对象转化为数组。
		///	</summary>
		///	<param name="obj" type="Object">需要转化为数组的对象。</param>
		///	<param name="_start" type="Number">进行截取，截取的起始索引。</param>
		///	<param name="_start" type="Number">需要截取的末尾索引。</param>
		return slice.call(obj, _start || 0, _end);
	};
}(
	Array.prototype.slice
));

this.merge = (function(nesting, toArray){
	return function(obj, args){
		///	<summary>
		///	深度合并对象中所有项。
		///	</summary>
		///	<param name="obj" type="Object, Array">需要合并的项。</param>
		///	<param name="args" type="Object, Array">其他需要合并的项列表。</param>
		if(
			obj instanceof Array
		){
			return obj
				.concat
				.apply(
					obj,
					toArray(arguments, 1)
				);
		}

		var result = {}, another = arguments[1];

		nesting(
			arguments,
			function(value, name){
				result[name] = typeof value === "object" ? this(value) : value;
			},
			arguments.callee
		);
		
		return result;
	};
}(
	this.nesting,
	this.toArray
));

this.toString = (function(){
	return function(){
		///	<summary>
		///	使函数在控制台里看起来像本地代码。
		///	</summary>
		var name = this.name;

		if(
			name
		){
			return "function " + name + "() { [native code] }";
		}

		return "function (){ [native code] }";
	};
}());

this.prototype = (function(prototype, forEach, define, defineProperties, getPrototypeOf){
	defineProperties(prototype, {
		assign : function(props){
			///	<summary>
			///	给该类的属性赋值。
			///	</summary>
			///	<param name="props" type="Object">包含一个或多个属性的键值对。</param>
			/// <returns>this</returns>
			forEach(
				props,
				function(val, name){
					if(typeof val === "undefined")
						return;

					this[name] = val;
				},
				this
			);

			return this;
		},
		getOwnClass : function(){
			///	<summary>
			///	获取自身类。
			///	</summary>
			return this.constructor.prototype;
		},
		getParentClass : function(){
			///	<summary>
			///	获取父类。
			///	</summary>
			return getPrototypeOf(this.getOwnClass());
		},
		isChildOf : function(AncestorClass){
			///	<summary>
			///	判断该类是否是指定类的子孙类。
			///	</summary>
			///	<param name="AncestorClass" type="jQun, Function">指定的类，或指定类的构造函数。</param>
			return this instanceof AncestorClass.constructor;
		},
		override : function(props, _descriptor){
			///	<summary>
			///	重写一个或多个属性的值。
			///	</summary>
			///	<param name="props" type="Object">包含一个或多个属性的键值对。</param>
			///	<param name="_descriptor" type="Object">被添加或修改属性的描述符。</param>
			this.props(props, _descriptor);

			return this;
		},
		props : function(props, _descriptor){
			///	<summary>
			///	将一个或多个属性添加到该类，并/或修改现有属性的特性。
			///	</summary>
			///	<param name="props" type="Object">包含一个或多个属性的键值对。</param>
			///	<param name="_descriptor" type="Object">被添加或修改属性的描述符。</param>
			defineProperties(this, props, _descriptor);

			return this;
		},
		toString : function(){
			///	<summary>
			///	对象字符串。
			///	</summary>
			return "[jQun " + this.constructor.name + "]";
		}
	});

	/*
		bug：(2013.08.20)目前有些浏览器不支持，如：手机QQ浏览器，手机百度浏览器
	*/

	try {
		define(
			prototype,
			"__proto__",
			Object.getOwnPropertyDescriptor(
				Object.prototype,
				"__proto__"
			),
			{ settable : true, gettable : true }
		);
	}
	catch(e){}

	return prototype;
}(
	Object.create(
		null,
		{
			constructor : {
				value : jQun,
				writable : true
			}
		}
	),
	this.forEach,
	this.define,
	this.defineProperties,
	Object.getPrototypeOf
));

with(
	this
){
	forEach(
		this,
		function(property, name){
			if(name !== "prototype"){
				property.toString = toString;
			}

			this[name] = property;
		},
		jQun
	);
}

}(
	jQun,
	Object,
	Array
);


// 面向对象的基础
new function(
	jQun, Function,
	getParentClass, defineProperties, forEach, set, toString,
	getDescriptor, freeze, console,
	getArgumentsString
){

this.Class = (function(AnonymousClass, ARG_LIST_REGX, ARG_REGX, getConstructor){
	function Class(_constructor, _name, _ParentClass){
		///	<summary>
		///	派生出一个类。
		///	</summary>
		///	<param name="_constructor" type="Function">源构造函数。</param>
		///	<param name="_name" type="String">构造函数的名称。</param>
		///	<param name="_ParentClass" type="Class, Interface">需要继承的父类或接口</param>
		var constructor = _constructor ? _constructor : AnonymousClass;

		return new jQun(
			getConstructor(
				constructor,
				_name ? _name : (constructor.name || AnonymousClass.name),
				constructor
					.toString()
					.match(
						ARG_LIST_REGX
					)[1]
					.match(
						ARG_REGX
					),
				_ParentClass ? _ParentClass.constructor.argumentNames : null
			),
			_ParentClass || this.getOwnClass()
		);
	};
	Class = new jQun(Class);

	Class.override({
		getParentClass : function(){
			///	<summary>
			///	获取父类。
			///	</summary>
			var Parent = getParentClass.call(this);

			return Parent === Class ? null : Parent;
		},
		toString : function(){
			///	<summary>
			///	对象字符串。
			///	</summary>
			return "[Class " + this.constructor.name + "]";
		}
	});

	return Class.constructor;
}(
	// AnonymousClass
	function AnonymousClass(){},
	// ARG_LIST_REGX
	/function[^\(]*\(([^\)]*)/,
	// ARG_REGX
	/([^\s\,]+)/g,
	// getConstructor
	function(source, name, argumentNames, parentArgumentNames){
		return defineProperties(
			getDescriptor(
				new Function([
					"return {",
						"get '" + name + "' (){",
							"var callee = arguments.callee, Parent = this.getParentClass.call(callee.prototype);",

							"if(",
								"Parent",
							"){",
								"Parent",
									".constructor",
									".call(",
										"this",
										getArgumentsString(
											parentArgumentNames,
											argumentNames
										),
									");",
							"}",
							"return callee.source.apply(this, arguments);",
						"}",
					" };"
				].join(""))(),
				name
			).get,
			{
				argumentNames : argumentNames,
				source : source
			}
		);
	}
));

this.Enum = (function(every){
	function Enum(data){
		///	<summary>
		///	枚举。
		///	</summary>
		if(
			data instanceof Array
		){
			forEach(
				data,
				function(val, i){
					this[val] = i;
				},
				this
			);
		}
		else {
			set(this, data);
		}

		freeze(this);
	};
	Enum = new jQun(Enum);

	Enum.props({
		every : function(fn, _this){
			///	<summary>
			///	确定枚举的所有成员是否满足指定的测试。
			///	</summary>
			///	<param name="fn" type="Function">用于测试对象成员的测试函数。</param>
			///	<param name="_this" type="*">指定测试函数的 this 对象。</param>
			return every(this, fn, _this);
		},
		getNameByValue : function(value, _shouldReturnCamelCasing){
			///	<summary>
			///	根据枚举值获取枚举名称。
			///	</summary>
			///	<param name="value" type="*">指定的枚举值。</param>
			///	<param name="value" type="*">是否应该返回驼峰式的值。</param>
			var name;

			this.every(function(v, n){
				if(value === v){
					name = n;
					return false;
				}
				
				return true;
			});

			return _shouldReturnCamelCasing ? name.substring(0, 1).toLowerCase() + name.substring(1) : name;
		},
		forEach : function(fn, _this){
			///	<summary>
			///	遍历枚举的所有成员并对其执行指定操作函数。
			///	</summary>
			///	<param name="fn" type="Function">指定操作的函数。</param>
			///	<param name="_this" type="*">指定操作函数的 this 对象。</param>
			return forEach(this, fn, _this);
		}
	});

	return Enum.constructor;
}(
	jQun.every
));

this.Interface = (function(Class, isInstanceOf, getConstructor, getName){
	function Interface(propertyNames, _Interface){
		///	<summary>
		///	定义接口。
		///	</summary>
		///	<param name="propertyNames" type="Array">该接口所规定拥有的属性名称。</param>
		///	<param name="_Interface" type="Interface, Class">需要继承的父接口或父类。</param>
		if(
			!isInstanceOf(this, Interface.constructor)
		){
			return;
		}

		var NewInterface = new Class(
			getConstructor(
				propertyNames,
				(_Interface || Interface).constructor.argumentNames || []
			),
			getName(propertyNames),
			_Interface || Interface
		);

		defineProperties(
			NewInterface.constructor.source,
			{ toString : toString }
		);

		return freeze(NewInterface);
	};
	Interface = new jQun(Interface);

	return Interface.constructor;
}(
	this.Class,
	jQun.isInstanceOf,
	// getConstructor
	function(propertyNames, argumentNames){
		return defineProperties(
			function(){
				if(
					propertyNames.length === 0
				){
					return;
				}

				var name;

				propertyNames.every(
					function(propertyName){
						if(
							propertyName in this
						){
							return true;
						}

						name = propertyName;
						return false;
					},
					this
				);

				if(
					typeof name !== "string"
				){
					return;
				}

				throw '接口 ' + this.toString() + ' ：必须定义属性 "' + name + '" ！';
			},
			{
				toString : function(){
					return "function (" + argumentNames.join(", ") + "){}";
				}
			}
		);
	},
	// getName
	function(propertyNames){
		return "Interface [" + propertyNames.join(", ") + "]";
	}
));

this.Namespace = (function(){
	function Namespace(){
		///	<summary>
		///	开辟一个命名空间。
		///	</summary>
	};
	Namespace = new jQun(Namespace);

	Namespace.props({
		members : function(members){
			///	<summary>
			///	给该命名空间赋予成员。
			///	</summary>
			return set(this, members);
		}
	});

	return Namespace.constructor;
}());

this.StaticClass = (function(AnonymousStaticClass, getConstructor){
	function StaticClass(_constructor, _name, _props, _descriptor){
		///	<summary>
		///	派生出一个静态类。
		///	</summary>
		///	<param name="_constructor" type="Function">源构造函数。</param>
		///	<param name="_name" type="String">构造函数的名称。</param>
		///	<param name="_props" type="Object">类的属性。</param>
		///	<param name="_descriptor" type="Object, Array">被添加属性的描述符。</param>
		var CreatedStaticClass,
		
			constructor = _constructor ? _constructor : AnonymousStaticClass;

		CreatedStaticClass = new jQun(
			getConstructor(
				_name ? _name : (constructor.name || AnonymousStaticClass.name)
			),
			this.getOwnClass()
		);

		if(
			_props
		){
			CreatedStaticClass.props(_props, _descriptor);
		}

		constructor.call(CreatedStaticClass);
		return CreatedStaticClass;
	};
	StaticClass = new jQun(StaticClass);

	StaticClass.override({
		getParentClass : function(){
			///	<summary>
			///	获取父类。
			///	</summary>
			return null;
		},
		toString : function(){
			///	<summary>
			///	对象字符串。
			///	</summary>
			return "[StaticClass " + this.constructor.name + "]";
		}
	});

	return StaticClass.constructor;
}(
	// AnonymousStaticClass
	function AnonymousStaticClass(){},
	// getConstructor
	function(name){
		return getDescriptor(
			new Function("return { get '" + name + "' (){} };")(),
			name
		).get;
	}
));

defineProperties(jQun, this);
}(
	jQun,
	Function,
	jQun.prototype.getParentClass,
	jQun.defineProperties,
	jQun.forEach,
	jQun.set,
	jQun.toString,
	Object.getOwnPropertyDescriptor,
	Object.freeze,
	console,
	// getArgumentsString
	function(parentArgumentNames, argumentNames){
		if(
			parentArgumentNames === null
		){
			return "";
		}

		if(
			argumentNames === null
		){
			return "";
		}

		return parentArgumentNames
			.map(
				function(name){
					var i = argumentNames.indexOf(name);

					if(
						i === -1
					){
						if(
							name.indexOf("_") === 0
						){
							i = argumentNames.indexOf(name.substring(1));
						}
					}

					return ", arguments[" + i + "]";
				}
			)
			.join("");
	}
);


new function(jQun, Class, StaticClass, Interface, Enum, defineProperties, forEach){

// 一些独立的基础类
(function(forEach){

this.OSs = (function(){
	return new Enum(
		["Unknow", "Windows", "Macintosh", "Linux", "Android", "IPhone", "IPad", "IPod", "WindowsPhone"]
	);
}());

this.BrowserCores = (function(){
	return new Enum(
		["Unknow", "Blink", "AppleWebKit", "Gecko", "Trident"]
	);
}());

this.BrowserAgents = (function(){
	return new Enum(
		["Unknow", "Chrome", "Firefox", "MSIE", "Safari", "Opera", "MicroMessenger", "MQQBrowser"]
	);
}());

this.Browser = (function(OSs, BrowserCores, BrowserAgents, userAgent, parseInt){
	function Browser(){
		var core, info = {};

		[
			/Mozilla\/[\d\.]+ \(([^; ]+).*(AppleWebKit)\/([\d\.]+).*(Chrome|MicroMessenger|MQQBrowser|Version)\/([\d\.]+)/,
			/Mozilla\/[\d\.]+ \(([^; ]+).*rv\s*\:\s*([\d\.]+).*(Gecko)\/[\d\.]+ ([^\/\s]+)\/([\d\.]+)/,
			/(MSIE) ([\d\.]+).*(Windows).*(Trident)\/([\d\.]+)/,
			/Mozilla\/[\d\.]+ \(([^; ]+).*(Trident)\/([\d\.]+).*rv\s*\:\s*([\d\.]+)/
		].every(function(regx, i){
			var result = userAgent.match(regx);

			if(
				result === null
			){
				return true;
			}

			result.splice(0, 1);

			this[i].forEach(
				function(key, i){
					var value = result[i];

					info[key] = key in this ? this[key][value.substring(0, 1).toUpperCase() + value.substring(1)] : value;
				},
				{ agent : BrowserAgents, core : BrowserCores, os : OSs }
			);

			return false;
		}, [
			["os", "core", "coreVersion", "agent", "version"],
			["os", "coreVersion", "core", "agent", "version"],
			["agent", "version", "os", "core", "coreVersion"],
			["os", "core", "coreVersion", "version"]
		]);

		core = info.core;

		if(
			core === BrowserCores.AppleWebKit
		){
			if(
				userAgent.indexOf("Android") > -1
			){
				info.os = OSs.Android;
			}

			if(
				info.agent === BrowserAgents.Chrome
			){
				if(
					parseInt(info.version) > 27
				){
					info.core = BrowserCores.Blink;
				}
			}
			else if(
				!info.agent
			){
				info.agent = BrowserAgents.Safari;
			}
		}
		else if(
			core === BrowserCores.Trident
		){
			info.agent = BrowserAgents[info.isMobile ? "WindowsPhone" : "MSIE"];
		}

		info.name = BrowserAgents.getNameByValue(info.agent);

		this.assign(info);
	};
	Browser = new StaticClass(Browser, "jQun.Browser", {
		// 浏览器代理商
		agent : BrowserAgents.Unknow,
		// 浏览器核心
		core : BrowserCores.Unknow,
		// 浏览器核心版本
		coreVersion : "0",
		// 浏览器名称
		name : BrowserAgents.getNameByValue(BrowserAgents.Unknow),
		// 操作系统
		os : OSs.Unknow,
		// 浏览器版本
		version : "0"
	});

	Browser.props({
		getMobileOSVersion : function(){
			///	<summary>
			///	获取移动操作系统的版本，精确到浮点数。
			///	</summary>
			var version;

			if(
				this.isMobile
			){
				var result = userAgent.match(this.os === OSs.Android ? /Android ([\d\.]+)/ : /OS ([\d\_]+)/);

				if(
					result
				){
					version = result[1].replace("_", ".");
				}
			}

			return parseFloat(version);
		},
		// 是否为移动端浏览器
		isMobile : userAgent.indexOf("Mobile") > -1
	});

	return Browser;
}(
	this.OSs,
	this.BrowserCores,
	this.BrowserAgents,
	navigator.userAgent,
	parseInt,
	parseFloat
));

this.List = (function(NaN, define, toArray, hasOwnProperty){
	function List(_list){
		///	<summary>
		///	对列表进行管理、操作的类。
		///	</summary>
		///	<param name="_list" type="List、Array">初始化时所拥有的列表集合。</param>
		this.assign({ length : 0 });

		if(
			!_list
		){
			return;
		}

		this.combine(_list);
	};
	List = new Class(List, "jQun.List");

	List.props({
		alternate : function(num, _remainder){
			///	<summary>
			///	交替性取出集合中的符合项。
			///	</summary>
			///	<param name="num" type="Number">取模运算值。</param>
			///	<param name="_remainder" type="Number">余数。</param>
			var list = this.createList();

			_remainder = _remainder || 0;

			this.forEach(function(item, i){
				if(
					i % num === _remainder
				){
					list.push(item);
				}
			});
			return list;
		},
		clear : function(){
			///	<summary>
			///	清空整个集合。
			///	</summary>
			this.splice(0);
			return this;
		},
		combine : function(list){
			///	<summary>
			///	合并另一个集合。
			///	</summary>
			///	<param name="list" type="List, Array">另一个集合。</param>
			this.push.apply(this, toArray(list));
			return this;
		},
		createList : function(){
			///	<summary>
			///	创建个新的列表。
			///	</summary>
			return new List.constructor();
		},
		distinct : function(){
			///	<summary>
			///	对列表进行去重。
			///	</summary>
			var list = this;

			this.splice(
				0
			).forEach(function(item){
				if(
					list.contains(item)
				){
					return;
				}

				list.push(item);
			});
			return list;
		},
		even : function(){
			///	<summary>
			///	返回集合中偶数项集合。
			///	</summary>
			return this.alternate(2);
		},
		length : NaN,
		odd : function(){
			///	<summary>
			///	返回集合中奇数项集合。
			///	</summary>
			return this.alternate(2, 1);
		}
	});
	
	
	Object.getOwnPropertyNames(
		Array.prototype
	).forEach(
		function(name){
			if(
				hasOwnProperty.call(List, name)
			){
				return;
			}

			if(
				name === "toString"
			){
				return;
			}

			define(List, name, this[name]);
		},
		Array.prototype
	);

	return List.constructor;
}(
	NaN,
	jQun.define,
	jQun.toArray,
	Object.prototype.hasOwnProperty
));

}.call(
	this
));


// 与字符串处理有关的类
(function(){

this.VerificationRegExpString = (function(EMAILS, WEB_URLS, toPerfectMatchString){
	return new Enum({
		Chinese : "[\\u4e00-\\u9fa5]",
		Email : toPerfectMatchString(EMAILS),
		Emails : EMAILS,
		Empty : "^$",
		NotEmpty : "[\\s\\S]",
		UserInfo : "^\\w{6,16}$",
		Telephone : "^(?:(?:\\d{3}|\\d{4})-)?(\\d{7,8}|\\d{11})$",
		WebURL : toPerfectMatchString(WEB_URLS),
		WebURLs : WEB_URLS
	});
}(
	// EMAILS
	"(\\w+(?:[-+.]\\w+)*)@(\\w+(?:[-.]\\w+)*)\\.(\\w+(?:[-.]\\w+)*)",
	// WEB_URLS
	"http:\\/\\/([\\w-]+)\\.+([\\w-]+)(?:\\/([\\w- .\\/?%&=]*))?",
	// toPerfectMatchString
	function(str){
		return "^" + str + "$";
	}
));

this.JSON = (function(JSONBase, Function, SPECIAL_CHARS_REGEXP){
	function JSON(){
		///	<summary>
		///	JSON功能类。
		///	</summary>
	};
	JSON = new StaticClass(JSON, "jQun.JSON");

	JSON.props({
		parse : function(jsonStr){
			///	<summary>
			///	将字符串转化为json。
			///	</summary>
			///	<param name="jsonStr" type="String">需要转化为json的字符串。</param>
			try {
				return JSONBase.parse(jsonStr);
			} catch(e){
				return JSONBase.parse(
					jsonStr.replace(
						SPECIAL_CHARS_REGEXP,
						"\\r\\n"
					)
				);
			}
		},
		stringify : JSONBase.stringify
	});

	return JSON;
}(
	JSON,
	Function,
	// SPECIAL_CHARS_REGEXP
	/[^\S ]/g
));

this.Text = (function(Array, T_REGX, encodeURIComponent){
	function Text(text){
		///	<summary>
		///	用于操作字符串文本的类。
		///	</summary>
		///	<param name="text" type="String, Array">字符串文本。</param>
		this.assign({
			text : text instanceof Array ? text.join("") : text
		});
	};
	Text = new Class(Text, "jQun.Text");

	Text.props({
		removeSpace : function(){
			///	<summary>
			///	 移除字符串中的前后空格。
			///	</summary>
			return this.text.match(/^\s*([\S\s]*?)\s*$/)[1];
		},
		replace : function(replacement){
			///	<summary>
			///	返回一个替换数据后的字符串。
			///	</summary>
			///	<param name="replacement" type="Object, Function">需要替换的数据或者自行替换的处理函数。</param>
			return this.text.replace(
				T_REGX,
				typeof replacement === "function" ?
					replacement :
					function(str, modifier, word){
						if(
							modifier === ":"
						){
							return "{" + word + "}";
						}

						var val = replacement[word];

						return val == null ? modifier === "~" ? "" : word : val;
					}
			);
		},
		toUrlParams : function(params){
			///	<summary>
			///	 返回一个替换数据后的连接字符串。
			///	</summary>
			///	<param name="params" type="Object, Function">需要替换的数据或者自行替换的处理函数。</param>
			return this.replace(function(str, modifier, word){
				return encodeURIComponent(params[word]);
			});
		},
		text : ""
	});

	return Text.constructor;
}(
	Array,
	// T_REGX
	/\{\s*(?:\?([^\{\}\s]{1}))?\s*([^\{\}]*?)\s*\}/g,
	encodeURIComponent
));

this.Verification = (function(VerificationRegExpString, RegExp){
	function Verification(){
		///	<summary>
		///	验证。
		///	</summary>
	};
	Verification = new StaticClass(Verification, "jQun.Verification");

	Verification.props({
		match : function(str, regExpString, _regxAttrs){
			///	<summary>
			///	验证匹配。
			///	</summary>
			///	<param name="str" type="String">需要验证的字符串。</param>
			///	<param name="regExpString" type="String">用于匹配的正则字符串。</param>
			///	<param name="_regxAttrs" type="String">正则属性。</param>
			return str.match(new RegExp(regExpString, _regxAttrs));
		},
		result : function(str, regExpString){
			///	<summary>
			///	验证结果。
			///	</summary>
			///	<param name="str" type="String">需要验证的字符串。</param>
			///	<param name="regExpString" type="String">用于匹配的正则字符串。</param>
			return !!this.match(str, regExpString);
		}
	});

	return Verification;
}(
	this.VerificationRegExpString,
	RegExp
));

}.call(
	this
));


// List的直接子类
(function(List){

this.IElementPropertyCollection = (function(){
	return new Interface(
		["propertyName"], List.prototype
	);
}());

this.ElementPropertyCollection = (function(IElementPropertyCollection){
	function ElementPropertyCollection(elementList){
		///	<summary>
		///	所有元素属性的基类。
		///	</summary>
		///	<param name="elementList" type="Array, List">元素列表。</param>
		var name = this.propertyName;

		this.assign({
			sources : elementList
		});

		if(
			name === ""
		){
			return;
		}

		elementList.forEach(
			function(element){
				this.push(element[name]);
			},
			this
		);
	};
	ElementPropertyCollection = new Class(ElementPropertyCollection, "jQun.ElementPropertyCollection", IElementPropertyCollection);

	ElementPropertyCollection.props({
		sources : null,
		valueOf : function(){
			///	<summary>
			///	返回当前对象。
			///	</summary>
			return this;
		}
	});

	return ElementPropertyCollection.constructor;
}(
	this.IElementPropertyCollection
));

this.PortList = (function(){
	function PortList(){
		///	<summary>
		///	与socket相关的端口列表。
		///	</summary>
	};
	PortList = new Class(PortList, "jQun.PortList", List.prototype);

	PortList.override({
		indexOf : function(name){
			///	<summary>
			///	检查指定名称的端口，返回索引值。
			///	</summary>
			///	<param name="name" type="String">端口的名称。</param>
			var index = -1;

			this.every(function(port, i){
				if(
					port.name === name
				){
					index = i;

					return false;
				}

				return true;
			});

			return index;
		}
	});

	return PortList.constructor;
}());

}.call(
	this,
	this.List
));


// 与ElementPropertyCollection（元素属性）有关的类
(function(ElementPropertyCollection){

this.AttributesCollection = (function(){
	function AttributesCollection(elementList){
		///	<summary>
		///	元素特性集合。
		///	</summary>
	};
	AttributesCollection = new Class(AttributesCollection, "jQun.AttributesCollection", ElementPropertyCollection.prototype);

	AttributesCollection.props({
		contains : function(name){
			///	<summary>
			///	判断是否包含指定名称的属性。
			///	</summary>
			///	<param name="name" type="String">属性的名称。</param>
			return !this
				.sources
				.every(function(element){
					return !element.hasAttribute(name);
				});
		},
		get : function(name){
			///	<summary>
			///	通过指定名称获取属性。
			///	</summary>
			///	<param name="name" type="String">需要获取属性的名称。</param>
			return this.sources[0].getAttribute(name);
		},
		propertyName : "attributes",
		set : function(name, _value){
			///	<summary>
			///	设置集合中所有元素的属性。
			///	</summary>
			///	<param name="name" type="String">需要设置属性的名称。</param>
			///	<param name="_value" type="*">需要设置属性的值。</param>
			if(
				_value == null
			){
				_value = "";
			}

			this.sources.forEach(function(element){
				element.setAttribute(name, _value);
			});
			return this;
		},
		remove : function(name){
			///	<summary>
			///	移除具有指定名称的属性。
			///	</summary>
			///	<param name="name" type="String">需要移除属性的名称。</param>
			this.sources
				.forEach(function(element){
					element.removeAttribute(name);
				});

			return this;
		},
		replace : function(oldAttrName, newAttrName, newAttrValue){
			///	<summary>
			///	移除指定的旧属性，添加指定的新属性。
			///	</summary>
			///	<param name="oldAttrName" type="String">需要移除属性的名称。</param>
			///	<param name="newAttrName" type="String">需要添加属性的名称。</param>
			///	<param name="newAttrValue" type="*">需要添加属性的值。</param>
			this.sources
				.forEach(function(element){
					element.removeAttribute(oldAttrName);
					element.setAttribute(newAttrName, newAttrValue);
				});

			return this;
		},
		toggle : function(name, _value){
			///	<summary>
			///	自行判断集合中每一个元素是否含有指定的属性：有则移除，无则添加并设置指定的值。
			///	</summary>
			///	<param name="name" type="String">需要设置属性的名称。</param>
			///	<param name="_value" type="*">需要设置属性的值。</param>
			if(
				this.contains(name)
			){
				return this.remove(name);
			}

			return this.set(name, _value);
		}
	});

	return AttributesCollection.constructor;
}());

this.CSSPropertyCollection = (function(isNaN, hasOwnProperty){
	function CSSPropertyCollection(elementList){
		///	<summary>
		///	元素CSS属性集合。
		///	</summary>
	};
	CSSPropertyCollection = new Class(CSSPropertyCollection, "jQun.CSSPropertyCollection", ElementPropertyCollection.prototype);

	CSSPropertyCollection.props({
		get : function(name){
			///	<summary>
			///	获取集合中第一个元素的CSS属性。
			///	</summary>
			///	<param name="name" type="String">CSS属性名。</param>
			return this[0][name];
		},
		propertyName : "style",
		set : function(name, value){
			///	<summary>
			///	设置集合中所有元素的CSS属性。
			///	</summary>
			///	<param name="props" type="Object">CSS属性键值对。</param>
			this.forEach(function(style){
				style[name] = value;
			});
			return this;
		}
	});

	forEach(
		getComputedStyle(document.documentElement),
		function(value, name, CSSStyle){
			// firefox、chrome 与 IE 的 CSSStyleDeclaration 结构都不一样
			var cssName = isNaN(name - 0) ? name : value;

			if(
				hasOwnProperty.call(CSSPropertyCollection, cssName)
			){
				return;
			}

			if(
				typeof CSSStyle[cssName] !== "string"
			){
				return;
			}

			var property = {};

			property[cssName] = {
				get : function(){
					return this.get(cssName);
				},
				set : function(value){
					this.set(cssName, value);
				}
			};

			CSSPropertyCollection.props(property, { gettable : true, settable : true });
		}
	);

	return CSSPropertyCollection.constructor;
}(
	isNaN,
	Object.prototype.hasOwnProperty
));

this.ChildrenCollection = (function(){
	function ChildrenCollection(elementList){
		///	<summary>
		///	children集合。
		///	</summary>
	};
	ChildrenCollection = new Class(ChildrenCollection, "jQun.ChildrenCollection", ElementPropertyCollection.prototype);

	ChildrenCollection.props({
		append : function(element){
			///	<summary>
			///	添加一个子元素。
			///	</summary>
			///	<param name="element" type="Element">需要添加的子元素。</param>
			return this.insert(element);
		},
		contains : function(element){
			///	<summary>
			///	返回一个布尔值，该值表示该集合内的所有子元素是否含有指定的元素。
			///	</summary>
			///	<param name="element" type="Element">可能包含的子元素。</param>
			return this.valueOf().indexOf(element) > -1;
		},
		insert : function(element, _idx){
			///	<summary>
			///	在指定的索引处插入元素。
			///	</summary>
			///	<param name="element" type="Element">需要插入的元素。</param>
			///	<param name="_idx" type="Number">指定的索引处。</param>
			var sources = this.sources;

			sources.insertTo.call([element], sources[0], _idx);
			return this;
		},
		propertyName : "children",
		remove : function(_element){
			///	<summary>
			///	移除指定的子元素，如果没指定子元素，则移除所有子元素。
			///	</summary>
			var children = this.valueOf();

			if(
				_element
			){
				var index = children.indexOf(_element);

				if(
					index > -1
				){
					children.splice(index, 1);
					children.remove();
				}
			}
			else {
				children.remove();
			}

			return this;
		},
		valueOf : function(){
			///	<summary>
			///	返回所有子元素的集合。
			///	</summary>
			return this.sources.query(">*");
		}
	});

	return ChildrenCollection.constructor;
}());

this.ClassListCollection = (function(){
	function ClassListCollection(elementList){
		///	<summary>
		///	classList集合。
		///	</summary>
	};
	ClassListCollection = new Class(ClassListCollection, "jQun.ClassListCollection", ElementPropertyCollection.prototype);

	ClassListCollection.props({
		add : function(className){
			///	<summary>
			///	为集合中每一个元素添加指定的单个class。
			///	</summary>
			///	<param name="className" type="String">指定的单个class。</param>
			this.forEach(function(classList){
				classList.add(className);
			});
			return this;
		},
		contains : function(className){
			///	<summary>
			///	判断集合中是否有一个元素包含指定的class。
			///	</summary>
			///	<param name="className" type="String">指定的单个class。</param>
			return !this.every(function(classList){
				return !classList.contains(className);
			});
		},
		propertyName : "classList",
		remove : function(className){
			///	<summary>
			///	为集合中每一个元素移除指定的单个class。
			///	</summary>
			///	<param name="className" type="String">指定的单个class。</param>
			this.forEach(function(classList){
				classList.remove(className);
			});
			return this;
		},
		replace : function(oldClass, newClass){
			///	<summary>
			///	为集合中每一个元素移除指定的旧class，添加指定的新class。
			///	</summary>
			///	<param name="oldClass" type="String">指定的旧class。</param>
			///	<param name="newClass" type="String">指定的新class。</param>
			this.forEach(function(classList){
				classList.remove(oldClass);
				classList.add(newClass);
			});
			return this;
		},
		toggle : function(className){
			///	<summary>
			///	自行判断集合中每一个元素是否含有指定的class：有则移除，无则添加。
			///	</summary>
			///	<param name="className" type="String">指定的单个class。</param>
			this.forEach(function(classList){
				classList.toggle(className);
			});
			return this;
		},
		valueOf : function(){
			///	<summary>
			///	返回集合中第一个元素的className。
			///	</summary>
			return this[0].toString();
		}
	});

	return ClassListCollection.constructor;
}());

this.DatasetCollection = (function(AttributesCollection, PREFIX){
	function DatasetCollection(elementList){
		///	<summary>
		///	dataset集合。
		///	</summary>
	};
	DatasetCollection = new Class(DatasetCollection, "jQun.DatasetCollection", AttributesCollection.prototype);

	DatasetCollection.override({
		contains : function(name){
			///	<summary>
			///	判断是否包含指定名称的数据。
			///	</summary>
			///	<param name="name" type="String">数据的名称。</param>
			return AttributesCollection.prototype.contains.call(this, PREFIX + name);
		},
		get : function(name){
			///	<summary>
			///	通过指定名称获取数据。
			///	</summary>
			///	<param name="name" type="String">需要获取数据的名称。</param>
			return AttributesCollection.prototype.get.call(this, PREFIX + name);
		},
		propertyName : "dataset",
		set : function(name, _value){
			///	<summary>
			///	设置集合中所有元素的数据。
			///	</summary>
			///	<param name="name" type="String">需要设置数据的名称。</param>
			///	<param name="_value" type="*">需要设置数据的值。</param>
			return AttributesCollection.prototype.set.call(this, PREFIX + name, _value);
		},
		remove : function(name){
			///	<summary>
			///	移除具有指定名称的数据。
			///	</summary>
			///	<param name="name" type="String">需要移除数据的名称。</param>
			return AttributesCollection.prototype.remove.call(this, PREFIX + name);
		},
		replace : function(oldDataName, newDataName, newDataValue){
			///	<summary>
			///	移除指定的旧数据，添加指定的新数据。
			///	</summary>
			///	<param name="oldDataName" type="String">需要移除数据的名称。</param>
			///	<param name="newDataName" type="String">需要添加数据的名称。</param>
			///	<param name="newDataValue" type="*">需要添加数据的值。</param>
			return AttributesCollection.prototype.replace.call(this, PREFIX + oldDataName, PREFIX + newDataName, newDataValue);
		}
	});

	return DatasetCollection.constructor;
}(
	this.AttributesCollection,
	// PREFIX
	"data-"
));

}.call(
	this,
	// AttributesCollection.prototype
	this.ElementPropertyCollection
));


// 与DOM有关的类
(function(
	List,
	AttributesCollection, CSSPropertyCollection, ChildrenCollection, ClassListCollection, DatasetCollection,
	Text, Window,
	forEach, define
){

this.EventTargetList = (function(toArray){
	function EventTargetList(_list){
		///	<summary>
		///	事件目标列表类。
		///	</summary>
	};
	EventTargetList = new Class(EventTargetList, "jQun.EventTargetList", List.prototype);

	EventTargetList.props({
		attach : function(events, _capture, _priority, _useWeakReference){
			///	<summary>
			///	向集合中所有目标注册事件侦听器。
			///	</summary>
			///	<param name="events" type="Object">事件侦听器键值对。</param>
			///	<param name="_capture" type="Boolean">侦听器是否运行于捕获阶段。</param>
			///	<param name="_priority" type="Number">优先级，数字越大，优先级越高。</param>
			///	<param name="_useWeakReference" type="Boolean">是否是属于强引用。</param>
			var eventTargetList = this, otherArgs = toArray(arguments, 1);
			
			forEach(
				events,
				function(fn, type){
					var eventArgs = [
							type,
							fn.length === 2 ?
								function(e){
									fn.call(
										this,
										e,
										eventTargetList.createList(e.target)
									);
								} :
								fn
						].concat(
							otherArgs
						);

					eventTargetList.forEach(function(eventTarget){
						eventTarget.addEventListener.apply(eventTarget, eventArgs);
					});
				}
			);

			return this;
		},
		detach : function(events){
			///	<summary>
			///	移除集合中所有目标的事件侦听器。
			///	</summary>
			///	<param name="events" type="Object">事件侦听器键值对。</param>
			this.forEach(function(eventTarget){
				forEach(
					events,
					function(fn, type){
						eventTarget.removeEventListener(type, fn);
					}
				);
			});

			return this;
		},
		dispatch : function(event){
			///	<summary>
			///	给所有目标分配事件。
			///	</summary>
			///	<param name="event" type="jQun.Event">事件。</param>
			this.forEach(function(eventTarget){
				event.trigger(eventTarget);
			});

			return this;
		}
	});

	return EventTargetList.constructor;
}(
	jQun.toArray
));

this.DOM = (function(EventTargetList, document, parseFloat){
	function DOM(){
		///	<summary>
		///	DOM类。
		///	</summary>
	};
	DOM = new StaticClass(DOM, "jQun.DOM");

	DOM.props({
		getCSSNumberValue : function(cssValue){
			///	<summary>
			///	对css的值进行处理，获取其数值部分。
			///	</summary>
			///	<param name="cssValue" type="String">css值。</param>
			return parseFloat(cssValue);
		},
		loaded : function(handler){
			///	<summary>
			///	当文档内容加载完毕的时候，所执行的函数。
			///	</summary>
			///	<param name="handler" type="Function">处理函数。</param>
			var docTarget = new EventTargetList([document]);

			docTarget.attach({
				DOMContentLoaded : function(){
					handler.apply(this, arguments);
					docTarget.detach({ DOMContentLoaded : arguments.callee });
				}
			});

			return this;
		}
	});

	return DOM;
}(
	this.EventTargetList,
	document,
	parseFloat
));

this.NodeList = (function(EventTargetList, DOCUMENT_POSITION_SAME, DOCUMENT_POSITION_CONTAINED_BY, DOCUMENT_POSITION_FOLLOWING_AND_CONTAINED_BY){
	function NodeList(_list){
		///	<summary>
		///	节点列表类。
		///	</summary>
	};
	NodeList = new Class(NodeList, "jQun.NodeList", EventTargetList.prototype);

	NodeList.override({
		createList : function(){
			///	<summary>
			///	创建个新的节点集合。
			///	</summary>
			return new NodeList.constructor();
		}
	});

	NodeList.props({
		appendTo : function(parentNode){
			///	<summary>
			///	将集合中所有节点添加至指定的父节点。
			///	</summary>
			///	<param name="parentNode" type="Node">指定的父节点。</param>
			this.insertTo(parentNode);
			return this;
		},
		contains : function(childNode){
			///	<summary>
			///	判断指定节点是否是该集合中某个节点的后代节点。
			///	</summary>
			///	<param name="childNode" type="Node">指定的节点。</param>
			return !this.every(function(node){
				return [
					DOCUMENT_POSITION_SAME,
					DOCUMENT_POSITION_CONTAINED_BY,
					DOCUMENT_POSITION_FOLLOWING_AND_CONTAINED_BY
				].indexOf(
					node.compareDocumentPosition(childNode)
				) === -1;
			});
		},
		insertAfter : function(targetNode){
			///	<summary>
			///	将集合中所有节点插入至指定的节点之后。
			///	</summary>
			///	<param name="targetNode" type="Node">指定节点。</param>
			var sibling = targetNode.nextElementSibling;

			if(
				sibling
			){
				this.insertBefore(sibling);
			}
			else {
				this.appendTo(targetNode.parentNode);
			}

			return this;
		},
		insertBefore : function(targetNode){
			///	<summary>
			///	将集合中所有节点插入至指定的节点之前。
			///	</summary>
			///	<param name="targetNode" type="Node">指定节点。</param>
			this.forEach(
				function(node){
					this.insertBefore(node, targetNode);
				},
				targetNode.parentNode
			);

			return this;
		},
		insertTo : function(parentNode, _idx){
			///	<summary>
			///	将集合中所有节点插入至指定索引的节点之前。
			///	</summary>
			///	<param name="parentNode" type="Node">指定的父节点。</param>
			///	<param name="_idx" type="Number">指定节点的索引值。</param>
			if(
				typeof _idx !== "undefined"
			){
				var childNodes = parentNode.childNodes;

				if(
					childNodes.length > 0
				){
					return this.insertBefore(childNodes[_idx]);
				}
			}

			this.forEach(function(node){
				parentNode.appendChild(node);
			});
			return this;
		},
		remove : function(){
			///	<summary>
			///	将集合中的节点从其父节点内移除。
			///	</summary>
			this.forEach(function(node){
				node.parentNode.removeChild(node);
			});
			return this;
		},
		replace : function(targetNode){
			///	<summary>
			///	将集合中所有节点去替换指定的节点。
			///	</summary>
			///	<param name="targetNode" type="Node">指定的节点。</param>
			this.insertBefore(targetNode);
			this.remove.call([targetNode]);

			return this;
		}
	});

	return NodeList.constructor;
}(
	this.EventTargetList,
	// DOCUMENT_POSITION_SAME
	0,
	// DOCUMENT_POSITION_CONTAINED_BY
	16,
	// DOCUMENT_POSITION_FOLLOWING_AND_CONTAINED_BY
	20
));

this.ElementList = (function(NodeList, Node, ID, selectorText, document){
	function ElementList(_selector, _parent){
		///	<summary>
		///	通过指定选择器筛选元素。
		///	</summary>
		///	<param name="_selector" type="String, Element">选择器或dom元素。</param>
		///	<param name="_parent" type="Element">指定查询的父节元素。</param>
		if(
			!_selector
		){
			return;
		}

		this.assign(
			{ selector : _selector }
		);

		if(
			typeof _selector === "string"
		){
			var parent = _parent || document;

			if(
				parent.id === ""
			){
				parent.setAttribute("id", ID);

				this.combine(
					parent.querySelectorAll(
						selectorText.replace(["#", ID, _selector])
					)
				);

				parent.removeAttribute("id");
			}
			else {
				this.combine(
					parent.querySelectorAll(
						selectorText.replace(
							parent === document ? ["", "", _selector] : ["#", parent.id, _selector]
						)
					)
				);
			}

			return;
		}

		if(
			_selector instanceof Node || _selector instanceof Window
		){
			if(
				_parent
			){
				if(
					_parent.contains(_selector) === false
				){
					return;
				}
			}

			this.push(_selector);
			return;
		}

		if(
			"length" in _selector
		){
			this.combine(_selector);
			return;
		}
	};
	ElementList = new Class(ElementList, "jQun.ElementList", NodeList.prototype);

	ElementList.override({
		createList : function(_selector, _parent){
			///	<summary>
			///	创建个新的元素集合。
			///	</summary>
			///	<param name="_selector" type="String, Element">选择器、html或dom元素。</param>
			///	<param name="_parent" type="Element">指定查询的父节点。</param>
			return new ElementList.constructor(_selector, _parent);
		}
	});

	ElementList.props({
		blur : function(){
			///	<summary>
			///	让聚焦元素的失去焦点。
			///	</summary>
			this.forEach(function(element){
				element.blur();
			});

			return this;
		},
		btw : function(_selector, _ancestor){
			///	<summary>
			///	在该集合内的每一个元素与指定的祖先元素之间，查找其他符合条件的元素。
			///	</summary>
			///	<param name="_selector" type="String">指定查找的祖先元素选择器。</param>
			///	<param name="_ancestor" type="Element">指定的一个祖先元素。</param>
			var list = [], elements = this.createList(_selector || "*", _ancestor);

			elements.forEach(
				function(element){
					this.every(function(elem){
						if(
							element.contains(elem)
						){
							list.push(element);
							return false;
						}

						return true;
					});
				},
				this
			);

			elements.splice(0);
			elements.combine(list);

			return elements;
		},
		del : function(name, _type){
			///	<summary>
			///	将指定属性从集合的所有元素中删除。
			///	</summary>
			///	<param name="name" type="String">需要删除的属性名。</param>
			///	<param name="_type" type="String">需要删除的属性种类。</param>
			this.forEach(function(element){
				delete element[name];
			});

			return this;
		},
		query : function(_selector){
			///	<summary>
			///	通过选择器查找子孙元素。
			///	</summary>
			///	<param name="_selector" type="String">选择器。</param>
			var source = ElementList.constructor.source, list = this.createList();

			this.forEach(function(Element){
				source.call(list, _selector, Element);
			});

			if(
				this.length < 2
			){
				return list;
			}

			return list.distinct();
		},
		focus : function(){
			///	<summary>
			///	聚焦元素。
			///	</summary>
			var length = this.length;

			if(
				length > 0
			){
				this[length - 1].focus();
			}

			return this;
		},
		get : function(name, _type){
			///	<summary>
			///	获取集合中第一个元素的属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			///	<param name="_type" type="String">需要获取的属性种类。</param>
			return this[0][name];
		},
		getAttribute : function(name){
			///	<summary>
			///	获取集合中第一个元素的特性属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			return AttributesCollection.prototype.get.call({ sources : this }, name);
		},
		getData : function(name){
			///	<summary>
			///	获取集合中第一个元素的数据值。
			///	</summary>
			///	<param name="name" type="String">数据名。</param>
			return DatasetCollection.prototype.get.call({ sources : this }, name);
		},
		isBtw : function(_selector, _ancestor){
			///	<summary>
			///	判断在该集合内的每一个元素与指定的祖先元素之间，是否能查找到其他符合条件的元素。
			///	</summary>
			///	<param name="_selector" type="String">指定查找的祖先元素选择器。</param>
			///	<param name="_ancestor" type="Element">指定的一个祖先元素。</param>
			return this.btw.apply(this, arguments).length > 0;
		},
		parent : function(){
			///	<summary>
			///	返回该集合所有元素的父元素。
			///	</summary>
			var list = this.createList();

			this.forEach(function(element){
				var parent = element.parentElement;

				if(!parent || list.contains(parent))
					return;

				list.push(parent);
			});
			return list;
		},
		removeAttribute : function(name){
			///	<summary>
			///	根据指定名称，移除集合中每一个元素的特性属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			AttributesCollection.prototype.remove.call({ sources : this }, name);
			return this;
		},
		removeData : function(name){
			///	<summary>
			///	根据指定名称，移除集合中每一个元素的数据。
			///	</summary>
			///	<param name="name" type="String">数据名。</param>
			DatasetCollection.prototype.remove.call({ sources : this }, name);
			return this;
		},
		selector : "",
		set : function(name, value, _type){
			///	<summary>
			///	设置集合中所有元素的属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			///	<param name="value" type="*">属性值。</param>
			///	<param name="_type" type="String">需要设置的属性种类。</param>
			this.forEach(function(element){
				element[name] = value;
			});
			return this;
		},
		setAttribute : function(name, _value){
			///	<summary>
			///	设置集合中每一个元素的特性属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			///	<param name="_value" type="String">属性值。</param>
			AttributesCollection.prototype.set.call({ sources : this }, name, _value);
			return this;
		},
		setData : function(name, _value){
			///	<summary>
			///	设置集合中第一个元素的数据值。
			///	</summary>
			///	<param name="name" type="String">数据名。</param>
			///	<param name="_value" type="String">数据值。</param>
			DatasetCollection.prototype.set.call({ sources : this }, name, _value);
			return this;
		}
	});

	ElementList.props({
		attributes : {
			get : function(){
				///	<summary>
				///	获取属性集合。
				///	</summary>
				return new AttributesCollection(this);
			},
			set : function(attrs){
				///	<summary>
				///	设置属性。
				///	</summary>
				///	<param name="attrs" type="Object">属性键值对。</param>
				var elementList = this;

				forEach(
					attrs,
					function(value, name){
						elementList.setAttribute(name, value);
					}
				);
			}
		},
		children : {
			get : function(){
				///	<summary>
				///	获取子元素集合。
				///	</summary>
				return new ChildrenCollection(this);
			},
			set : function(elements){
				///	<summary>
				///	移除所有现有子元素，添加指定的子元素。
				///	</summary>
				///	<param name="elements" type="Array, jQun.NodeList">需要添加的子元素集合。</param>
				this.children.remove();
				this.constructor(elementList).appendTo(this[0]);
			}
		},
		dataset : {
			get : function(){
				///	<summary>
				///	获取数据集合。
				///	</summary>
				return new DatasetCollection(this);
			},
			set : function(data){
				///	<summary>
				///	设置数据。
				///	</summary>
				///	<param name="data" type="Object">数据键值对。</param>
				var elementList = this; 

				forEach(
					data,
					function(value, name){
						elementList.setData(name, value);
					}
				);
			}
		}
	}, { gettable : true, settable : true });

	forEach(
		["header", "article", "section", "footer"],
		function(name){
			var property = {};
			
			property[name] = {
				get : function(){
					return this.query(">" + name);
				},
				set : function(element){
					this.createList(element).replace(this[name][0]);
				}
			};

			ElementList.props(property, this);
		},
		{ gettable : true, settable : true }
	);

	return ElementList.constructor;
}(
	this.NodeList,
	Node,
	// ID
	"__jQun__",
	// selectorText
	new Text("{0}{1} {2}"),
	document
));

this.HTMLElementList = (function(ElementList, getCSSNumberValue, getComputedStyle, addProperty){
	function HTMLElementList(_selector, _parent){
		///	<summary>
		///	通过指定选择器筛选HTML元素。
		///	</summary>
		///	<param name="_selector" type="String, Element">选择器或dom元素。</param>
		///	<param name="_parent" type="Element">指定查询的父元素。</param>
	};
	HTMLElementList = new Class(HTMLElementList, "jQun.HTMLElementList", ElementList.prototype);

	forEach(
		[
			"className", "hidden", "href",
			"id", "innerHTML", "src",
			"tabIndex", "title", "value"
		],
		addProperty,
		HTMLElementList
	);

	HTMLElementList.override({
		createList : function(_selector, _parent){
			///	<summary>
			///	创建个新的HTML元素集合。
			///	</summary>
			///	<param name="_selector" type="String, Element">选择器或dom元素。</param>
			///	<param name="_parent" type="Element">指定查询的父元素。</param>
			return new HTMLElementList.constructor(_selector, _parent);
		}
	});

	HTMLElementList.props({
		classList : {
			get : function(){
				///	<summary>
				///	获取class列表集合。
				///	</summary>
				return new ClassListCollection(this);
			},
			set : function(className){
				///	<summary>
				///	设置集合中所有元素的class属性。
				///	</summary>
				///	<param name="className" type="String">需要设置的class字符串。</param>
				this.set("className", className);
			}
		},
		height : {
			get : function(){
				///	<summary>
				///	获取集合中每一个元素的高。
				///	</summary>
				return this.getMetrics("height");
			},
			set : function(height){
				///	<summary>
				///	设置集合中每一个元素的高。
				///	</summary>
				///	<param name="height" type="String, Number">元素的高。</param>
				this.setMetrics("height", height);
			}
		},
		style : {
			get : function(){
				///	<summary>
				///	获取style属性集合。
				///	</summary>
				return new CSSPropertyCollection(this);
			},
			set : function(cssText){
				///	<summary>
				///	设置集合中每一个元素的style属性。
				///	</summary>
				///	<param name="cssText" type="String">需要设置的style属性字符串。</param>
				AttributesCollection
					.prototype
					.set
					.call(
						{ sources : this },
						"style",
						cssText
					);
			}
		},
		width : {
			get : function(){
				///	<summary>
				///	获取集合中每一个元素的宽。
				///	</summary>
				return this.getMetrics("width");
			},
			set : function(width){
				///	<summary>
				///	设置集合中每一个元素的宽。
				///	</summary>
				///	<param name="width" type="String, Number">元素的宽。</param>
				this.setMetrics("width", width);
			}
		}
	}, { gettable : true, settable : true });

	HTMLElementList.props({
		getCSSPropertyValue : function(name){
			///	<summary>
			///	获取集合中第一个元素的css属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			return getComputedStyle(this[0])[name];
		},
		getMetrics : function(name){
			///	<summary>
			///	获取元素指定盒模型属性值。
			///	</summary>
			///	<param name="name" type="String">盒模型属性名称。</param>
			return getCSSNumberValue(this.getCSSPropertyValue(name));
		},
		hasTag : function(tagName){
			///	<summary>
			///	判断集合类是否含有指定标签名称的元素。
			///	</summary>
			///	<param name="tagName" type="String">标签名称。</param>
			tagName = tagName.toUpperCase();

			return !this.every(function(htmlElement){
				return htmlElement.tagName !== tagName;
			});
		},
		hide : function(){
			///	<summary>
			///	隐藏元素。
			///	</summary>
			return this.setAttribute("hidden", "");
		},
		rect : function(_name){
			///	<summary>
			///	获取第一个元素的客户端属性。
			///	</summary>
			///	<param name="_name" type="String">需要只返回单个属性值的属性名称。</param>
			var rect = this[0].getBoundingClientRect();

			return _name in rect ? rect[_name] : rect;
		},
		setCSSPropertyValue : function(name, value){
			///	<summary>
			///	设置集合中每一个元素的css属性。
			///	</summary>
			///	<param name="name" type="String">属性名。</param>
			///	<param name="value" type="String">属性值。</param>
			this.forEach(function(element){
				element.style[name] = value;
			});
			return this;
		},
		setMetrics : function(name, _value){
			///	<summary>
			///	设置元素指定盒模型属性值。
			///	</summary>
			///	<param name="name" type="String">盒模型属性名称。</param>
			///	<param name="_value" type="String, Number">盒模型属性值。</param>
			if(
				typeof _value === "number"
			){
				_value += "px";
			}

			this.setCSSPropertyValue(name, _value);
			return this;
		},
		show : function(){
			///	<summary>
			///	显示元素。
			///	</summary>
			///	<param name="_display" type="String">修改元素display的css值。</param>
			return this.removeAttribute("hidden");
		},
		toggle : function(){
			///	<summary>
			///	显示或隐藏元素。
			///	</summary>
			return this[this.getAttribute("hidden") === null ? "hide" : "show"]();
		}
	});

	return HTMLElementList.constructor;
}(
	this.ElementList,
	this.DOM.getCSSNumberValue,
	getComputedStyle,
	// addProperty
	function(name){
		define(this, name, {
			get : function(){
				return this.get(name);
			},
			set : function(value){
				this.set(name, value);
			}
		}, { gettable : true, settable : true });
	}
));

}.call(
	this,
	this.List,
	this.AttributesCollection,
	this.CSSPropertyCollection,
	this.ChildrenCollection,
	this.ClassListCollection,
	this.DatasetCollection,
	this.Text,
	// Window
	window.constructor,
	forEach,
	jQun.define
));


// 与HTMLElementList相关的类
(function(HTMLElementList, Text, Browser, BrowserAgents, document){

this.HTML = (function(Function, SPACE_REGEXP, FOR_REGEXP, WORD_TEXT, replace, toArray, console){
	function HTML(template){
		///	<summary>
		///	html模板。
		///	</summary>
		///	<param name="template" type="String, HTMLScriptElement">html模板源字符串或script、noscript标签。</param>
		this.assign({
			template : [
				"with(this){ return (function(){ this.push('",
				// 使用Text类的replace替换参数
				replace.call({
					text : (typeof template === "string" ? template : template.textContent)
						// 给单引号加保护
						.split("'")
						.join("\\'")
						// 替换掉特殊的空白字符
						.replace(
							SPACE_REGEXP,
							""
						)
						// 替换for循环
						.replace(
							FOR_REGEXP,
							function(str, condition, i){
								return [
									"');forEach(",
									condition
										.split("{")
										.join("\t")
										.split("}")
										.join("\n"),
									", function(" + (i || "") + ")\t this.push('"
								].join("");
							}
						)
				}, function(str, modifier, word){
					if(
						modifier === ":"
					){
						return "\t" + word + "\n";
					}

					if(
						modifier === ">"
					){
						return "');console.log(" + word + ");this.push('";
					}

					return replace.call(
						{ text : WORD_TEXT },
						{ word : word, shouldEmpty : modifier === "~" }
					);
				})
				// 替换for循环的结束标识“}”
				.split("}")
				.join("');}, this);this.push('")
				// 替换临时产生的大括号
				.split("\t")
				.join("{")
				.split("\n")
				.join("}"),
				"');return this.join('');}.call([])); }"
			].join("")
		});
	};
	HTML = new Class(HTML, "jQun.HTML");

	HTML.props({
		create : function(_data, _shouldAsArray){
			///	<summary>
			///	将模板转化为html节点。
			///	</summary>
			///	<param name="data" type="Object, Array">需要渲染的数据。</param>
			///	<param name="_shouldAsArray" type="Boolean">是否以普通数组形式返回节点。</param>
			var parent = document.createElement("div");

			parent.innerHTML = this.render(_data);

			if(
				_shouldAsArray
			){
				var childNodes = toArray(parent.childNodes);
				
				parent.innerHTML = "";
				return childNodes;
			}

			var htmlElementList = new HTMLElementList("");

			htmlElementList.combine(parent.childNodes);
			htmlElementList.remove();

			return htmlElementList;
		},
		render : function(_data){
			///	<summary>
			///	渲染模板。
			///	</summary>
			///	<param name="_data" type="Object, Array">需要渲染的数据。</param>
			return new Function(
				"forEach",
				"console",
				this.template
			).call(
				_data || {},
				forEach,
				console
			);
		},
		template : ""
	});

	return HTML.constructor;
}(
	Function,
	// SPACE_REGEXP => space(查找特殊的空白字符)
	/[\r\t\n]/g,
	// FOR_REGEXP => for(查找for语句)
	/@for\s*\(([\s\S]+?)(?:\s*->>\s*([\s\S]+?))*?\)\s*\{/g,
	// WORD_TEXT
	"');this.push(typeof ({word}) === 'undefined' ? ({shouldEmpty} ? '' : '{word}') : {word});this.push('",
	Text.prototype.replace,
	jQun.toArray,
	console
));

this.Event = (function(eventTargetClasses, div, attach, contains, define, set){
	function Event(name, _type, _initEventArgs, _init){
		///	<summary>
		///	DOM事件类。
		///	</summary>
		///	<param name="name" type="String">事件名称。</param>
		///	<param name="_type" type="String">事件类型(MouseEvent、UIEvent、WheelEvent等)。</param>
		///	<param name="_initEventArgs" type="Array">初始化事件的其他参数列表。</param>
		///	<param name="_init" type="Function">事件初始化函数。</param>
		this.assign({
			initEventArgs : [name].concat(_initEventArgs || [true, true]),
			name : name,
			type : _type
		});

		if(
			typeof _init !== "function"
		){
			return;
		}
		
		_init.call(this);
	};
	Event = new Class(Event, "jQun.Event");

	Event.props({
		attachTo : function(target){
			///	<summary>
			///	应该附加该事件的标签。
			///	</summary>
			///	<param name="target" type="String, Node">标签名称。</param>
			var t = [], name = this.name;

			if(
				typeof target === "string"
			){
				if(
					target === "*"
				){
					t = eventTargetClasses;
				}
				else {
					t.push(document.createElement(target).constructor.prototype);
				}
			}
			else {
				t.push(target);
			}

			forEach(
				t,
				function(tg){
					define(tg, "on" + name, this,	{ settable : true, gettable : true });
				},
				{
					get : function(){
						return null;
					},
					set : function(fn){
						var obj = {};

						obj[name] = fn;

						attach.call(
							this instanceof HTMLElementList ? this : [this],
							obj
						);
					}
				}
			);

			return this;
		},
		eventAttrs : null,
		initEventArgs : null,
		name : "",
		setAttrs : function(attrs){
			///	<summary>
			///	设置事件属性。
			///	</summary>
			///	<param name="attrs" type="Object">属性键值对。</param>
			this.eventAttrs = attrs;
			return this;
		},
		source : null,
		trigger : function(target){
			///	<summary>
			///	触发事件。
			///	</summary>
			///	<param name="target" type="Node">触发该事件的节点。</param>
			var type = this.type, event = document.createEvent(type);

			event[
				"init" + type
			].apply(
				event,
				this.initEventArgs
			);

			set(event, this.eventAttrs);

			/*
				2014.5.26 chrome bug：
				因为chrome的捕获事件必须由文档开始；
				所以未添加到文档中的元素，不会有事件冒泡，IE、FF正常。
			*/
			if(
				Browser.agent === BrowserAgents.Chrome
			){
				if(
					target !== document.documentElement
				){
					var body = document.body;

					if(
						contains.call([body], target) === false
					){
						var elem = target;

						while(
							elem.parentElement
						){
							elem = elem.parentElement;
						}

						div.appendChild(elem);
						body.appendChild(div);
						target.dispatchEvent(event);

						div.innerHTML = "";

						body.removeChild(div);
						return this;
					}
				}
			}

			target.dispatchEvent(event);
			return this;
		},
		type : "Event"
	});

	return Event.constructor;
}(
	// eventTargetClasses
	[
		HTMLElementList.prototype
	].concat(
		"EventTarget" in window ? [EventTarget.prototype] : [Node.prototype, window.constructor.prototype]
	),
	// div
	new this.HTML(
		'<div style="display:none !important;"></div>'
	).create(
		null
	)[0],
	HTMLElementList.prototype.attach,
	HTMLElementList.prototype.contains,
	jQun.define,
	jQun.set
));

}.call(
	this,
	this.HTMLElementList,
	this.Text,
	this.Browser,
	this.BrowserAgents,
	document
));


// 与 HTML 相关的类
(function(HTML, ElementList, Text, DOM, JSON){
	
this.CHTML = (function(selectorText, array, contentRegx, parse){
	function CHTML(){
		///	<summary>
		///	组件html模板(Conponent HTML)：如果里面嵌入了JSON字符串，那么将其转化为JSON数据，作为参数传入类的构造函数。
		///	</summary>
		array.indexOf = function(name){
			var i = 0;

			this.every(function(item, n){
				if(
					item.name === name
				){
					i = n;
					return false;
				}

				return true;
			});

			return i;
		};

		DOM.loaded(
			function(){
				new ElementList(
					array
						.map(function(item){
							return selectorText.replace(item);
						})
						.join(", ")
				).forEach(function(script, i){
					var scriptList = new ElementList(script),
				
						result = scriptList.get("textContent").match(contentRegx);

					new array[
						array.indexOf(
							scriptList.getData("name")
						)
					].Class(
						result === null ? null : parse(result[1])
					).insertBefore(script);

					scriptList.remove();
				});
			},
			true
		);
	};
	CHTML = new StaticClass(CHTML, "jQun.CHTML");

	CHTML.props({
		bind : function(Class, name){
			///	<summary>
			///	绑定模板的类与名称。
			///	</summary>
			///	<param name="Class" type="jQun.Class">需要绑定的类。</param>
			///	<param name="name" type="String">需要绑定的名称。</param>
			array.push({ Class : Class, name : name });

			return this;
		}
	});

	return CHTML;
}(
	// selectorText
	new Text([
		'script[type="text/chtml"][data-name="{name}"]',
		'noscript[type="text/chtml"][data-name="{name}"]'
	].join(",")),
	// array
	[],
	// contentRegx
	/^\s*(\S[\s\S]*?)\s*$/,
	JSON.parse
));

this.SHTML = (function(){
	function SHTML(){
		///	<summary>
		///	静态html模板(Static HTML)。
		///	</summary>
		DOM.loaded(function(){
			new ElementList([
				'script[type="text/shtml"]:not(:empty)',
				'noscript[type="text/shtml"]:not(:empty)'
			].join(",")).forEach(function(htmlElement, i){
				new HTML(
					htmlElement.textContent
				)
				.create()
				.replace(
					htmlElement
				);
			});
		}, true);
	};
	SHTML = new StaticClass(SHTML, "jQun.SHTML");

	return SHTML;
}());

}.call(
	this,
	this.HTML,
	this.ElementList,
	this.Text,
	this.DOM,
	this.JSON
));


// 与数据有关的类
(function(JSON){

this.SessionCache = (function(window){
	function SessionCache(name){
		///	<summary>
		///	缓存数据。
		///	</summary>
		/// <param name="name" type="String">缓存数据的标识名称</param>
		this.assign({
			name : name
		});
	};
	SessionCache = new Class(SessionCache, "jQun.SessionCache");

	SessionCache.props({
		del : function(key){
			///	<summary>
			///	删除某一条缓存数据。
			///	</summary>
			/// <param name="key" type="String">缓存数据的主键</param>
			var storage = this.get();

			delete storage[key];

			window
				.sessionStorage
				.setItem(
					this.name,
					JSON.stringify(storage)
				);

			return this;
		},
		get : function(_key){
			///	<summary>
			///	获取某一条缓存数据。
			///	</summary>
			/// <param name="_key" type="String">缓存数据的主键</param>
			var storage = JSON.parse(window.sessionStorage.getItem(this.name));

			if(
				!storage
			){
				storage = {};
			}

			if(
				typeof _key === "undefined"
			){
				return storage;
			}

			return storage[_key];
		},
		name : "",
		set : function(key, value){
			///	<summary>
			///	设置某一条缓存数据。
			///	</summary>
			/// <param name="key" type="String">缓存数据的主键</param>
			/// <param name="value" type="Object, String, Number, Boolean">缓存数据的值</param>
			var storage = this.get();

			storage[key] = value;

			window
				.sessionStorage
				.setItem(
					this.name,
					JSON.stringify(storage)
				);

			return this;
		}
	});

	return SessionCache.constructor;
}(
	window // iphone safari，不能将sessionStorage赋予给其他变量..
));

this.Storage = (function(forEach){
	function Storage(){ };
	Storage = new Class(Storage, "jQun.Storage");

	Storage.props({
		clear : function(){
			///	<summary>
			///	清空所有储存数据。
			///	</summary>
			forEach(
				this,
				function(value, key){
					this.del(key);
				},
				this
			);

			return this;
		},
		del : function(key){
			///	<summary>
			///	删除一项储存数据。
			///	</summary>
			///	<param name="key" type="String">数据主键。</param>
			return delete this[key];
		},
		get : function(key){
			///	<summary>
			///	获取数据。
			///	</summary>
			///	<param name="key" type="String">数据主键。</param>
			return this[key];
		},
		set : function(key, value){
			///	<summary>
			///	设置数据。
			///	</summary>
			///	<param name="key" type="String">数据主键。</param>
			///	<param name="value" type="*">数据值。</param>
			this[key] = value;
			return this;
		}
	});

	return Storage.constructor;
}());

}.call(
	this,
	this.JSON,
	forEach
));


// 与ajax相关的类
(function(Storage, JSON, XMLHttpRequest, REQUEST_NAME,forEach, encodeURIComponent){

this.ConnectionTypes = (function(){
	return new Enum(
		["Get", "Post"]
	);
}());

this.ResponseTypes = (function(){
	return new Enum(
		["Text", "Json", "ArrayBuffer", "Document", "Blob"]
	);
}());

this.RequestConnection = (function(ConnectionTypes, ResponseTypes, implements, getEncodedParams){
	function RequestConnection(name, url, _Interface, _type, _responseType){
		///	<summary>
		///	ajax请求连接。
		///	</summary>
		///	<param name="name" type="String">连接名称。</param>
		///	<param name="url" type="String">连接url。</param>
		///	<param name="_Interface" type="Interface">发送数据的接口规范。</param>
		///	<param name="_type" type="ConnectionTypes">发送数据的方式。</param>
		///	<param name="_responseType" type="ResponseTypes">返回的数据格式。</param>
		this.assign({
			Interface : _Interface,
			name : name,
			responseType : _responseType,
			type : _type,
			url : url
		});
	};
	RequestConnection = new Class(RequestConnection, "jQun.RequestConnection");

	RequestConnection.props({
		Interface : null,
		create : function(urlQueryString, _formData, _load, _beforeSend, _isTesting){
			///	<summary>
			///	创建ajax连接。
			///	</summary>
			///	<param name="urlQueryString" type="String">url查询参数字符串。</param>
			///	<param name="_formData" type="FormData">需要上传的formData。</param>
			///	<param name="_load" type="Function">异步完成后所执行的回调函数。</param>
			///	<param name="_beforeSend" type="Function">在发送请求之前的回调函数。</param>
			///	<param name="_isTesting" type="Boolean">是否在测试环境中。</param>
			var request = new XMLHttpRequest();

			if(
				_isTesting
			){
				if(
					_load
				){
					_load(this.responseType === ResponseTypes.Json ? {} : "");
				}

				return request;
			}

			var responseType = this.responseType;

			request.open(
				ConnectionTypes.getNameByValue(
					_formData ? ConnectionTypes.Post : this.type,
					true
				),
				this.url + (urlQueryString ? "?" + urlQueryString : ""),
				true
			);

			try{
				// QQ浏览器下设置json等其他不支持的type，会报错
				request.responseType = ResponseTypes.getNameByValue(responseType, true);
			}
			catch(e){}

			if(
				_load
			){
				request.addEventListener(
					"load",
					function(){
						var response = this.response;

						if(
							this.status !== 200
						){
							return;
						}

						if(
							responseType === ResponseTypes.Json
						){
							// ie不能设置json，需要转换
							if(
								typeof response === "string"
							){
								response = JSON.parse(response);
							}
						}
						
						_load(response);
					}
				);
			}

			if(
				_beforeSend
			){
				_beforeSend(request);
			}

			request.send(_formData);

			return request;
		},
		name : "",
		open : function(params, _load, _isTesting){
			///	<summary>
			///	开打一个ajax连接。
			///	</summary>
			///	<param name="params" type="Object">url的替换参数及post方法的传递参数。</param>
			///	<param name="_load" type="Function">异步完成后所执行的回调函数。</param>
			///	<param name="_isTesting" type="Boolean">是否在测试环境中。</param>
			implements(this.Interface, params);

			if(
				this.type === ConnectionTypes.Post
			){
				var formData = new FormData();

				formData.append(REQUEST_NAME, this.name);

				forEach(
					params,
					function(value, name){
						formData.append(name, value);
					}
				);

				return this.create("", formData, _load, null, _isTesting);
			}

			return this.create(getEncodedParams(this.name, params), null, _load, null, _isTesting);
		},
		responseType : ResponseTypes.Json,
		type : ConnectionTypes.Get,
		upload : function(blob, _load, _progress, _isTesting){
			///	<summary>
			///	使用ajax上传文件。
			///	</summary>
			///	<param name="blob" type="Blob">需要上传的二进制数据，一般是文件。</param>
			///	<param name="_load" type="Function">异步完成后所执行的回调函数。</param>
			///	<param name="_progress" type="Function">数据上传进度回调函数。</param>
			///	<param name="_isTesting" type="Boolean">是否在测试环境中。</param>
			var formData = new FormData();

			formData.append(REQUEST_NAME, this.name);
			formData.append("blob", blob);

			return this.create(
				"",
				formData,
				_load,
				_progress ?
					function(request){
						request.addEventListener(
							"progress",
							function(e){
								var loaded = e.loaded, total = e.total;

								_progress(
									(loaded / total).toFixed(2) - 0,
									loaded,
									total
								);
							}
						);
					} :
					null,
				_isTesting
			);
		},
		url : ""
	});

	return RequestConnection.constructor;
}(
	this.ConnectionTypes,
	this.ResponseTypes,
	// implements
	function(Interface, params){
		if(
			!Interface
		){
			return;
		}

		new (
			new Class(
				null,
				"jQun.Ajax.CheckParameters",
				Interface
			)
			.props(params)
			.constructor
		);
	},
	// getEncodedParams
	function(name, params){
		var arr = [REQUEST_NAME + "=" + name];

		forEach(
			params,
			function(value, name){
				arr.push("&");

				if(
					typeof value === "object"
				){
					value = JSON.stringify(value);
				}

				arr.push(encodeURIComponent(name) + "=" + encodeURIComponent(value));
			}
		);

		return arr.join("");
	}
));

this.Ajax = (function(RequestConnection, Blob, ResponseTypes, toArray, console, location){
	function Ajax(){
		///	<summary>
		///	ajax异步类。
		///	</summary>
		if(
			!!XMLHttpRequest
		){
			this.enabled = true;
			return;
		}

		console.warn("当前浏览器不支持XMLHttpRequest。");
	};
	Ajax = new StaticClass(Ajax, "jQun.Ajax", { enabled : false });

	Ajax.props({
		beginTesting : function(){
			///	<summary>
			///	开始启动测试模式。
			///	</summary>
			this.isTesting = true;
			return this;
		},
		isTesting : false,
		open : function(name, data, _load, _progress){
			///	<summary>
			///	打开一个ajax连接。
			///	</summary>
			///	<param name="name" type="String">连接名称。</param>
			///	<param name="data" type="Object, Blob">url的替换参数及post方法的传递参数 或者 二进制数据（一般是文件）。</param>
			///	<param name="_load" type="Function">异步完成后所执行的回调函数。</param>
			///	<param name="_progress" type="Function">数据上传进度回调函数。</param>
			if(
				!this.enabled
			){
				return;
			}

			var requestConnection = this.requestStorage.get(name);

			if(
				!requestConnection
			){
				throw 'Ajax请求信息错误：请检查连接名称"' + name + '"是否正确。';
				return;
			}

			if(
				data instanceof Blob
			){
				return requestConnection.upload(data, _load, _progress, this.isTesting);
			}

			return requestConnection.open(data, _load, this.isTesting);
		},
		requestStorage : new Storage(),
		requestURLPrefix : [location.protocol, "", location.host, ""].join("/"),
		save : function(allSettings){
			///	<summary>
			///	存储ajax连接信息。
			///	</summary>
			///	<param name="allSettings" type="Array">ajax连接信息。</param>
			var requestStorage = this.requestStorage, requestURLPrefix = this.requestURLPrefix;

			forEach(
				allSettings,
				function(settings){
					var name = settings[0];

					requestStorage.set(
						name,
						new RequestConnection(
							name,
							requestURLPrefix + settings[1],
							settings[2],
							settings[3],
							settings[4]
						)
					);
				}
			);

			return requestStorage;
		},
		setRequestURLPrefix : function(urlPrefix){
			///	<summary>
			///	设置请求连接地址的前缀。
			///	</summary>
			///	<param name="urlPrefix" type="String">地址前缀。</param>
			this.requestURLPrefix = urlPrefix;
			return this;
		}
	});

	return Ajax;
}(
	this.RequestConnection,
	Blob,
	this.ResponseTypes,
	jQun.toArray,
	console,
	location
));

}.call(
	this,
	this.Storage,
	this.JSON,
	XMLHttpRequest,
	// REQUEST_NAME
	"__request__",
	forEach,
	encodeURIComponent
));


// 与socket相关的类
(function(HTMLElementList, TYPE, portList){

this.Socket = (function(window, onmessage){
	function Socket(){
		///	<summary>
		///	建立窗口之间的接口。
		///	</summary>
	};
	Socket = new StaticClass(Socket, "jQun.Socket");

	Socket.props({
		close : function(){
			///	<summary>
			///	关闭接口。
			///	</summary>
			if(
				this.closed
			){
				return;
			}

			new HTMLElementList(window)
				.detach(
					{ message : onmessage }
				);

			this.closed = true;
			return this;
		},
		closed : true,
		open : function(){
			///	<summary>
			///	开打接口。
			///	</summary>
			if(
				!this.closed
			){
				return;
			}

			new HTMLElementList(window)
				.attach(
					{ message : onmessage }
				);

			this.closed = false;
			return this;
		}
	});

	return Socket;
}(
	window,
	// onmessage
	function(e){
		var data = e.data;

		if(
			!data
		){
			return;
		}

		if(
			data.type !== TYPE
		){
			return;
		}

		var index = portList.indexOf(data.name);

		if(
			index === -1
		){
			return;
		}

		var handler = portList[index][data.key];

		if(
			!handler
		){
			return;
		}

		handler(data.value);
	}
));

this.Port = (function(){
	function Port(name){
		///	<summary>
		///	建立窗体之间的端口。
		///	</summary>
		///	<param name="name" type="String">端口名称。</param>
		this.assign({
			name : name
		});

		this.start();
	};
	Port = new Class(Port, "jQun.Port");

	Port.props({
		bind : function(window){
			///	<summary>
			///	绑定相关窗口。
			///	</summary>
			///	<param name="window" type="Window">相关（发送信息）的窗口。</param>
			this.window = window;

			return this;
		},
		enabled : false,
		listen : function(key, listener){
			///	<summary>
			///	监听信息。
			///	</summary>
			///	<param name="key" type="String">信息关键字。</param>
			///	<param name="listener" type="Function">信息处理函数。</param>
			this[key] = listener;

			return this;
		},
		name : "",
		post : function(key, _value, _window){
			///	<summary>
			///	发送信息。
			///	</summary>
			///	<param name="key" type="String">信息关键字。</param>
			///	<param name="_value" type="String">信息的值。</param>
			///	<param name="_window" type="Window">相关（发送信息）的窗口。</param>
			if(
				!this.enabled
			){
				return this;
			}

			(
				_window || this.window
			).postMessage(
				{
					key : key,
					name : this.name,
					type : TYPE,
					value : _value
				},
				"*"
			);

			return this;
		},
		start : function(){
			///	<summary>
			///	启动端口服务。
			///	</summary>
			if(
				this.enabled
			){
				return this;
			}
			
			portList.push(this);

			this.enabled = true;
			return this;
		},
		stop : function(){
			///	<summary>
			///	停止端口服务。
			///	</summary>
			if(
				!this.enabled
			){
				return this;
			}

			portList.splice(portList.indexOf(this.name), 1);

			this.enabled = false;
			return this;
		},
		window : null
	});

	return Port.constructor;
}());

}.call(
	this,
	this.HTMLElementList,
	// TYPE
	"__jQun.Port__",
	// portList
	new this.PortList()
));

defineProperties(jQun, this);
}(
	jQun,
	jQun.Class,
	jQun.StaticClass,
	jQun.Interface,
	jQun.Enum,
	jQun.defineProperties,
	jQun.forEach
);
