﻿1.1.1.9 将ElementList的get、set方法移至NodeList，并对NodeList增加属性textContent。

1.1.1.8 修复在IE下，HTML.create方法创建元素时，当未添加到文档中的时候，使用innerHTML会清空子孙元素内容的bug。

1.1.1.7 仅修改Event参数名称及某些文档架构，此次改动均不造成任何使用影响。

1.1.1.6 修改HTML的create方法第二种参数模式的返回值，将其变为普通数组。

1.1.1.5 修复CHTML选择器问题。

1.1.1.4 HTML类的create方法支持返回单个元素，目的是为了提高某些情况下的效率。

1.1.1.3 修复chrome下Event触发的bug，修复QQ浏览器ajax路径报错。

1.1.1.2 优化RequestConnection代码；将所有ajax请求的默认返回数据格式设置为json；移除RequestHeader类。

1.1.1.1 ajax修改，支持上传文件api。

1.1.1.0 修复NodeList.contains在IE下不能判断非元素节点（Text、Comment等）的问题；修复ElementList选择器未加保护问题；