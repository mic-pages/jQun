﻿1.0.4.9 支持子类继承父类的时候，可以将父类的可选参数做为必选参数（省下划线）。

1.0.4.8 增加__proto__属性，因为chrome 28以后，__proto__被归于Object.protoype之下，所以导致监控无法查看父类属性，增加__proto__的目的就是为了修复此bug。

1.0.4.7 Ajax缓存

1.0.4.6 ElementList增加属性：selector，用于记录传入参数selector的值，目的是为了方便监控错误

1.0.4.5 解决jQun.Event.prototype.attachTo的bug

1.0.4.4 解决jQun.HTML参数带有单引号出错的问题

1.0.4.3 jQun.prototype.getParentClass获取错误的修复

1.0.4.2 添加jQun.Event类，用于自定义事件

1.0.4.1 将本类库所有方法及类伪造成本地代码

1.0.4.0 类库大改版，将所有双层命名空间形式换成单层