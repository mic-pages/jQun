﻿1.0.3.1 jQun.forEach支持数字循环，简化HTML的"替换for循环函数"。

1.0.3.0 新增NodeList.hasChild:判断指定节点是否是该集合中某个元素的后代节点。

1.0.2.9 新增HTMLElementList.rect方法，以获取clientRect

1.0.2.8 List.createList堆栈溢出问题的解决

1.0.2.7 由于HTMLElementList.hidden属性的不理想，提供HTMLElementList.show、HTMLElementList.hide这2个方法

1.0.2.6 jQun.HTML开始支持id参数。如： new jQun.HTML("script_html", true); 第二参数为true(说明第一个参数为id)

1.0.2.5 修正了CSSPropertyCollection赋予数字出错问题

1.0.2.4 修改HTMLElement.metrics方法不能正确处理小数问题

1.0.2.3 静态类赋予成员的方式的修改

1.0.2.2 Browser添加判断手机浏览器类型

1.0.2.1 Ajax添加beginTesting,解决手机测试使用问题

1.0.2.0 增加html模板