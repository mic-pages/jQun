﻿1.0.7.7 修复ajax返回值的bug；增加requestURLPrefix属性及setRequestURLPrefix方法，用于控制请求地址前缀。

1.0.7.6 修复every数字方法的bug；HTMLElementList类增加hasTag方法，用于判断是否含有指定标签名称的元素。

1.0.7.5 新增DOM类；List、NodeList新增参数_list(初始化时所拥有的列表集合)。

1.0.7.4 推出Interface（接口）类，以模拟oop关键字interface；JSON类的完善安全；删除ElementList的sibling、siblings2个方法。

1.0.7.3 修复ajax的几个bug

1.0.7.2 增加socket相关类

1.0.7.1 1. 将NonstaticClass改为Class，而NonstaticClass也将保留，指向Class。 2. 向安卓(4.0.2)webview提供更多兼容

1.0.7.0 本类库结构大改及优化。