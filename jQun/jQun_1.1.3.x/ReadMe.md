﻿1.1.3.6 给JSON.parseAddressParameters增加自动decodeURIComponent功能；Storage.del方法，修复返回一直为true的bug。

1.1.3.5 修复Port不提供参数也会执行回调事件的bug；Enum增加方法：has，判断是否有指定名称的枚举。

1.1.3.4 Ajax.open增加参数：_error，用于错误回调函数；_beforeSend用于发送请求之前的回调函数。

1.1.3.3 移除mobile.js并加入mobile里面的2个自定义事件（userclick、shake）和一个editor.js里面的事件（reposcursor）。

1.1.3.2 同步缓存类，即实例化N个，不管先后顺序，数据共享（互相影响，即修改1个，其他同名且同类缓存也会被修改）。

1.1.3.1 获取元素属性（元素属性、css、节点属性等等）时，当元素个数为0时，设置返回默认值。

1.1.3.0 JSON类新增方法stringifyAddressParameters，并修改Ajax相关链接转义。